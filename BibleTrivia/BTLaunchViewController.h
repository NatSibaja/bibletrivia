//
//  BTLaunchViewController.h
//  BibleTrivia
//
//  Created by Natalia Sibaja on 3/11/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BTLaunchViewController : GAITrackedViewController
@property (strong, nonatomic) IBOutlet UIButton *facebookButton;
@end
