//
//  BTAdvertisingDataSource.h
//  BibleTrivia
//
//  Created by Natalia Sibaja on 4/7/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Ad;


@interface BTAdvertisingDataSource : NSObject
@property (nonatomic,strong) id delegate;

-(void)requestAds;

@end
