//
//  Books.m
//  BibleTrivia
//
//  Created by Leonardo Ortiz on 4/9/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import "Books.h"
#import "Application.h"


@implementation Books

@dynamic bookId;
@dynamic bookName;
@dynamic chapterCount;
@dynamic application;

@end
