//
//  BTSidebarViewController.h
//  BibleTrivia
//
//  Created by Natalia Sibaja on 3/17/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UAirship.h"
#import "UAInbox.h"
#import "UAInboxMessageListController.h"
#import "UAInboxMessageViewController.h"
#import "UALandingPageOverlayController.h"
#import "InboxSampleNavigationUserInterface.h"
#import "UAInboxAlertHandler.h"
#import "UAUtils.h"
#import "UAInboxPushHandler.h"

@interface SWUITableViewCell : UITableViewCell<UAInboxPushHandlerDelegate>

@end
@interface BTSidebarViewController : UITableViewController
@property(nonatomic, assign) BOOL useOverlay;
@property(nonatomic, assign) CGSize popoverSize;

@end
