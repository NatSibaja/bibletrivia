//
//  BTRegisterViewController.m
//  BibleTrivia
//
//  Created by Natalia Sibaja on 3/11/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import "BTRegisterViewController.h"
#import "UIButton+BTButton.h"
#import "BTUserDataSource.h"
#import "BTUserDelegate.h"
#import "User.h"
#import "User+BTUser.h"
#import "BTCustomActivityView.h"
#import "BTBannerView.h"
@interface BTRegisterViewController ()<UITextFieldDelegate, BTUserDelegate>

@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *confirmTextField;
@property (weak, nonatomic) IBOutlet UITextField *firstNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *zipTextField;
@property (weak, nonatomic) IBOutlet UIButton *subscribeButton;
@property (weak, nonatomic) IBOutlet BTCustomActivityView *activityView;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *fadeView;
@property (strong, nonatomic) User *user;

- (IBAction)registerTouched:(id)sender;
- (IBAction)termsTouched:(id)sender;
- (IBAction)viewTapped:(id)sender;

@end

@implementation BTRegisterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupView];
    [self setupTextFieldPlaceHolders];
   
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    self.screenName = @"Register Screen";
    [Flurry logEvent:@"Register Screen"];
}
-(void)setupView
{
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]];
    [self.fadeView setBackgroundColor:BLUE_FADE];
}
-(IBAction)subscribeClicked:(id)sender{
    if ([self.zipTextField isFirstResponder]) {
        [self.scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
        [self.zipTextField resignFirstResponder];
    }
    self.subscribeButton.selected = !self.subscribeButton.selected;
}

-(void)setupTextFieldPlaceHolders
{
    UIColor *color = [UIColor whiteColor];
    self.emailTextField.delegate = self;
    self.passwordTextField.delegate = self;
    self.confirmTextField.delegate = self;
    self.firstNameTextField.delegate = self;
    self.lastNameTextField.delegate = self;
    self.zipTextField.delegate = self;
    self.emailTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email Address" attributes:@{NSForegroundColorAttributeName: color}];
    self.passwordTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{NSForegroundColorAttributeName: color}];
    self.confirmTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Confirm Password" attributes:@{NSForegroundColorAttributeName: color}];
    self.firstNameTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"First Name" attributes:@{NSForegroundColorAttributeName: color}];
    self.lastNameTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Last Name" attributes:@{NSForegroundColorAttributeName: color}];
    self.zipTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"ZIP/ Postal Code" attributes:@{NSForegroundColorAttributeName: color}];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 50, 20)];
    self.emailTextField.leftView = paddingView;
    self.emailTextField.leftViewMode = UITextFieldViewModeAlways;

    self.passwordTextField.layer.sublayerTransform = CATransform3DMakeTranslation(50, 0, 0);
    self.confirmTextField.layer.sublayerTransform = CATransform3DMakeTranslation(50, 0, 0);
    self.firstNameTextField.layer.sublayerTransform = CATransform3DMakeTranslation(50, 0, 0);
    self.lastNameTextField.layer.sublayerTransform = CATransform3DMakeTranslation(50, 0, 0);
    self.zipTextField.layer.sublayerTransform = CATransform3DMakeTranslation(50, 0, 0);
    [self.loginButton fillGradientBackground:DARK_GREEN :LIGHT_GREEN];
}
#pragma mark - UITextField Delegate Methods

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    CGPoint pt;
    CGRect rc = [textField bounds];
    rc = [textField convertRect:rc toView:self.scrollView];
    pt = rc.origin;
    pt.x = 0;
    pt.y -= 60;
    [self.scrollView setContentOffset:pt animated:YES];
}
-(BOOL)textFieldShouldReturn:(UITextField*)textField;
{
    NSInteger nextTag = textField.tag + 1;
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        [nextResponder becomeFirstResponder];
    } else {
        CGPoint pt;
        pt.x = 0;
        pt.y = 0;
        [self.scrollView setContentOffset:pt animated:YES];
        [textField resignFirstResponder];
    }
    return NO; // We do not want UITextField to insert line-breaks.
}
- (IBAction)registerTouched:(id)sender {
    [self.activityView startAnimating];
    [self.zipTextField resignFirstResponder];
    [self.scrollView setContentOffset:CGPointMake(0,0) animated:YES];

    BTUserDataSource *datasource = [[BTUserDataSource alloc] init];
    datasource.delegate = self;
    self.user = [datasource getUserFromContext];
    if (self.user == nil){
        self.user = [datasource insertUserInContext];
    }
    self.user.firstName = self.firstNameTextField.text;
    self.user.lastName = self.lastNameTextField.text;
    self.user.email = self.emailTextField.text;
    self.user.password = self.passwordTextField.text;
    self.user.postalCode = self.zipTextField.text;
    self.user.subscribe = [NSNumber numberWithBool:self.subscribeButton.selected];
    [self createUser];
}
- (IBAction)termsTouched:(id)sender
{
    NSURL *url = [NSURL URLWithString: TERMS_URL];
    [[UIApplication sharedApplication] openURL:url];
}

- (IBAction)viewTapped:(id)sender
{
    CGPoint pt;
    pt.x = 0;
    pt.y = 0;
    [self.scrollView setContentOffset:pt animated:YES];
    [[self view] endEditing:TRUE];
}

-(void)createUser
{
    BTUserDataSource *datasource = [[BTUserDataSource alloc] init];
    datasource.delegate = self;
    [datasource addUserIntoServer:self.user];
}
-(void)didAddedUser:(NSDictionary *)userData
{
    if (self.user.subscribe) {
        BTUserDataSource *datasource = [[BTUserDataSource alloc] init];
        datasource.delegate = self;
        [datasource subscribeNewsletter:self.user];
    }
    [self.activityView stopAnimating];
    [self performSelectorOnMainThread:@selector(saveNewUser:) withObject:userData waitUntilDone:YES];
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Success" message:@"Your user has been created. You can review your information in the Account Settings menu." delegate:self cancelButtonTitle:NSLocalizedString(@"OK_BUTTON",@"") otherButtonTitles:nil];
    [alert show];
    [self performSegueWithIdentifier:@"mainSegue" sender:self];
}
-(void)didAddedUserError:(NSString *)errorMessage
{
    [self.activityView stopAnimating];
    [self.user erase];
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:errorMessage delegate:self cancelButtonTitle:@"Try Again" otherButtonTitles:nil];
    [alert show];
    [self.emailTextField becomeFirstResponder];
}
-(void)didSubscribe:(NSDictionary *)userData
{
    NSLog(@"user subscribed");
}
-(void)didSubscribeError:(NSDictionary *)userData
{
    NSLog(@"Error on subscribe user");

}
-(void)saveNewUser:(NSDictionary *)userData
{
    BTUserDataSource *datasource = [[BTUserDataSource alloc] init];
    datasource.delegate = self;
    self.user = [datasource getUserFromContext];
    if (self.user == nil){
        self.user = [datasource insertUserInContext];
    }
    self.user.authenticationKey = [userData objectForKey:@"userKey"];
    self.user.email = [userData objectForKey:@"email"];
    self.user.firstName = [userData objectForKey:@"firstName"];
    self.user.lastName = [userData objectForKey:@"lastName"];
    self.user.postalCode = [userData objectForKey:@"postalCode"];
    self.user.userId = [userData objectForKey:@"userid"];
    [self.user save];
}


@end
