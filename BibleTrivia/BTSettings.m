//
//  BTSettings.m
//  TriviaGame
//
//  Created by Natalia Sibaja on 4/7/15.
//  Copyright (c) 2015 Salem Web Network. All rights reserved.
//

#import "BTSettings.h"

@implementation BTSettings

#pragma mark - Class Methods

+ (instancetype)defaultSettings
{
    static BTSettings *settings = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        settings = [[self alloc] init];
    });
    return settings;
}

#pragma mark - Properties

static NSString *const kShouldSkipLoginKey = @"shouldSkipLogin";

- (BOOL)shouldSkipLogin
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:kShouldSkipLoginKey];
}

- (void)setShouldSkipLogin:(BOOL)shouldSkipLogin
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:shouldSkipLogin forKey:kShouldSkipLoginKey];
    [defaults synchronize];
}

@end