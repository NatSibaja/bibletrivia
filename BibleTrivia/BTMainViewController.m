//
//  BTMainViewController.m
//  BibleTrivia
//
//  Created by Natalia Sibaja on 2/19/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import "BTMainViewController.h"
#import "BTUserDataSource.h"
#import "User+BTUser.h"
#import "User.h"
#import "BTSplashAdViewController.h"

@interface BTMainViewController ()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sloganHeight;
@property (nonatomic) IBOutlet UIBarButtonItem* revealButtonItem;
@property (weak, nonatomic) IBOutlet UIView *bibleView;
@property (weak, nonatomic) IBOutlet UIButton *playButton;
@property (weak, nonatomic) IBOutlet UIButton *questionButton;
@property (weak, nonatomic) IBOutlet UIButton *aboutButton;
@property (weak, nonatomic) User *user;

-(IBAction)aboutButtonTouched:(id)sender;
@end

@implementation BTMainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupView];
    [self setupSideBar];
}
-(void)viewWillAppear:(BOOL)animated
{
    self.screenName = @"Welcome Screen";
    [Flurry logEvent:@"Welcome Screen"];
}
-(void)setupView
{
    //if 3.5 screen squeze logo into place
    if(!isiPhone5)
    {
        self.logoHeight.constant = 6;
        self.titleHeight.constant = 6;
        self.sloganHeight.constant = 3;
    }
        
    BTUserDataSource *datasource = [[BTUserDataSource alloc]init];
    datasource.delegate = self;
    self.user = [datasource getUserFromContext];
    if (self.user == nil) {
        [self.navigationItem setTitle:[NSString stringWithFormat:@"Welcome, Guest"]];
    }
    else{
        [self.navigationItem setTitle:[NSString stringWithFormat:@"Welcome, %@",self.user.firstName]];
    }
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.hidesBackButton = YES;
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]];
    [self.bibleView setBackgroundColor:BLUE_BUTTON];
    [self.playButton setBackgroundColor:BLUE_BUTTON];
    [self.questionButton setBackgroundColor:BLUE_BUTTON];
    [self.aboutButton setBackgroundColor:BLUE_BUTTON];
}
-(void)setupSideBar
{
    [self.revealButtonItem setTarget: self.revealViewController];
    [self.revealButtonItem setAction: @selector(revealToggle:)];
    [self.navigationController.navigationBar addGestureRecognizer: self.revealViewController.panGestureRecognizer];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)aboutButtonTouched:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Free Bible Trivia Quiz" message:@"Learn the Bible better and have fun with the Free Bible Trivia Quiz Game. Your entire family can learn about important people, places and events in Christian faith and memorize Scripture with our game!" delegate:self cancelButtonTitle:NSLocalizedString(@"CLOSE_BUTTON", @"") otherButtonTitles: nil];
    [alert show];
}
@end
