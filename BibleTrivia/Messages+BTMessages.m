//
//  Messages+BTMessages.m
//  TriviaGame
//
//  Created by Natalia Sibaja on 4/25/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import "Messages+BTMessages.h"
#import "BTManagedObjectContext.h"

@implementation Messages (BTMessages)
-(void)save{
    NSError *error = nil;
    if (![[BTManagedObjectContext sharedInstance].managedObjectContext  save:&error]) {
        if (DEBUG_COREDATA) {
            NSLog(@"CoreData:Error saving the message.%@",error);
        }
    }
}

-(void)erase{
    NSError *error = nil;
    [[BTManagedObjectContext sharedInstance].managedObjectContext deleteObject:self];
    if (![[BTManagedObjectContext sharedInstance].managedObjectContext  save:&error]) {
        if (DEBUG_COREDATA) {
            NSLog(@"CoreData:Failed deleting the message.%@",error);
        }
    }
}
@end
