//
//  BTSubmissionViewController.h
//  BibleTrivia
//
//  Created by Natalia Sibaja on 2/25/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BTSubmissionViewController : GAITrackedViewController<UITextFieldDelegate, UITextViewDelegate>
{
    //scrollview position
    CGPoint svos;
}
@end
