//
//  BTTriviaDataSource.m
//  BibleTrivia
//
//  Created by Natalia Sibaja on 3/31/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import "BTTriviaDataSource.h"
#import "Trivia+BTTrivia.h"
#import "BTManagedObjectContext.h"

@implementation BTTriviaDataSource

-(void)saveInContext:(Trivia *)trivia{
    [trivia save];
}
-(void)eraseFromContext:(Trivia *)trivia{
    [trivia erase];
}

-(Trivia *)getTriviaFromContext{
    NSFetchRequest * request = [[NSFetchRequest alloc]init];
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"Trivia"
                                                         inManagedObjectContext:[BTManagedObjectContext sharedInstance].managedObjectContext];
    [request setEntity:entityDescription];
    NSError *error=nil;
    NSArray * mutableFetchResults = [[BTManagedObjectContext sharedInstance].managedObjectContext executeFetchRequest:request error:&error];
    if (mutableFetchResults == nil) {
        if (DEBUG_COREDATA&&DEBUG_ENABLED) {
            NSLog(@"CoreData:Error selecting the trivia");
        }
        return nil;
    }
    else{
        if ([mutableFetchResults count]>0) {
            return [mutableFetchResults objectAtIndex:0];
        }
        else{
            return nil;
        }
    }
}

-(Trivia *)insertTriviaInContext{
    Trivia *trivia = nil;
    trivia = [NSEntityDescription insertNewObjectForEntityForName:@"Trivia"
                                         inManagedObjectContext:[BTManagedObjectContext sharedInstance].managedObjectContext];
    return trivia;
}

@end
