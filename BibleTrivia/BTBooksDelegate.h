//
//  BTBooksDelegate.h
//  BibleTrivia
//
//  Created by Leonardo Ortiz on 3/24/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Books.h"

@protocol BTBooksDelegate <NSObject>

-(void)didReceiveBooks:(NSArray *)data;
-(void)didFailedReceivingBooks:(NSString *)errorMessage;
@optional
//Returns an array of books. The method requestBooks must have been called at leat once
-(NSArray *)getAllBooks;
//Returns a book by id. The method requestBooks must have been called at leat once
-(Books *)getBookById:(NSNumber *)bookId;
//Request the books from the server, or from the local DB.
-(void)requestBooks;

@end
