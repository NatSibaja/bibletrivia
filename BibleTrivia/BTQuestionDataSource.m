//
//  BTQuestionDataSource.m
//  BibleTrivia
//
//  Created by Natalia Sibaja on 2/21/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import "BTQuestionDataSource.h"
#import "BTUrlSession.h"
#import "NSData+Additions.h"
#import "BTUserDataSource.h"
#import "User.h"

#define RECEIVEDATA_SELECTOR @"didReceiveQuestion:"
#define FAILED_SELECTOR @"didReceiveQuestionError:"
#define ERROR_MESSAGE @"Error in Question"

@implementation BTQuestionDataSource

#pragma mark -
#pragma mark Request question
-(void)requestQuestion
{
    BTUrlSession *btUrlSession = [[BTUrlSession alloc]init];
    btUrlSession.delegate = self;
    btUrlSession.didReceiveData = NSSelectorFromString(RECEIVEDATA_SELECTOR);
    btUrlSession.didReceiveError = NSSelectorFromString(FAILED_SELECTOR);
    
    BTUserDataSource *userDataSource = [[BTUserDataSource alloc]init];
    User *user = [userDataSource getUserFromContext];
    NSString *authStr = [NSString stringWithFormat:@"%@:", API_KEY];
    if (user != nil)
    {
        authStr = [NSString stringWithFormat:@"%@:%@", API_KEY,user.authenticationKey];

    }
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", [authData base64Encoding]];
    NSString *stringUrl = [NSString stringWithFormat:@"%@%@/singlerandom", API_URL,QUESTION_URL];
    [btUrlSession doGetRequest:stringUrl withParams:nil andHeaders:@{@"Content-Type":@"application/text",@"Authorization":authValue}];
}
-(void)didReceiveQuestion:(NSData *)data
{
    NSError *error;
    NSDictionary *dataDicc = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    if (!dataDicc) {
        [self didReceiveQuestionError:JSON_PARSING_ERROR];
        if (DEBUG_ENABLED&&DEBUG_QUESTIONS) {
            NSLog(@"Error parsing the question data");
        }
    }
    else{
        if ([self.delegate respondsToSelector:@selector(didReceiveQuestion:)]) {
            [self.delegate performSelector:@selector(didReceiveQuestion:) withObject:dataDicc];
        }
        else if (DEBUG_ENABLED&&DEBUG_QUESTIONS) {
            NSLog(@"Error parsing the question data");
        }
    }
}
-(void)didReceiveQuestionError:(NSUrlErrorType)error{
    if ([self.delegate respondsToSelector:@selector(didReceiveQuestionError:)]) {
        [self.delegate performSelector:@selector(didReceiveQuestionError:) withObject:ERROR_MESSAGE];
    }
    if (DEBUG_ENABLED&&DEBUG_QUESTIONS) {
        NSLog(@"Error getting the question data from the server");
    }
}

#pragma mark -
#pragma mark add new question


-(void)addQuestion:(BTQuestion *)question{
    BTUserDataSource *userDataSource = [[BTUserDataSource alloc]init];
    User *user = [userDataSource getUserFromContext];
    BTUrlSession *btUrlSession = [[BTUrlSession alloc]init];
    btUrlSession.delegate = self;
    btUrlSession.didReceiveData = NSSelectorFromString(@"didAddedQuestion:");
    btUrlSession.didReceiveError = NSSelectorFromString(@"didFailedAddingQuestion:");
    
    NSString *authStr = [NSString stringWithFormat:@"%@:%@", API_KEY,user.authenticationKey];
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", [authData base64Encoding]];
    NSString *stringUrl = [NSString stringWithFormat:@"%@%@", API_URL,QUESTION_URL];
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:user.userId forKey:@"userId"];
    [params setObject:user.firstName forKey:@"firstName"];
    [params setObject:user.lastName forKey:@"lastName"];
    [params setObject:user.email forKey:@"email"];
    [params setObject:question.questionText forKey:@"question"];
    [params setObject:question.answer forKey:@"answer"];
    [params setObject:question.choice1 forKey:@"choice1"];
    [params setObject:question.choice2 forKey:@"choice2"];
    [params setObject:question.choice3 forKey:@"choice3"];
    [params setObject:question.choice4 forKey:@"choice4"];
    [params setObject:question.bookId forKey:@"bookId"];
    [params setObject:question.chapter forKey:@"chapter"];
    [params setObject:question.startVerse forKey:@"startVerse"];
    [params setObject:question.endVerse forKey:@"endVerse"];
    [params setObject:question.bibleReferenceText forKey:@"comment"];
    [params setObject:@"newQuestionFromUser" forKey:@"resourceObjectType"];
    [btUrlSession doPostRequest:stringUrl withParams:params andHeaders:@{@"Content-Type":@"application/json",@"Authorization":authValue} withParamsFormat:JsonObject];
}

-(void)didAddedQuestion:(NSData *)data{
    NSError *error;
    NSDictionary *dataDicc = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    if (error==nil) {
        NSString *message = [[dataDicc objectForKey:@"resultType"]objectForKey:@"message"];
        if ([message isEqualToString:@"Success"]) {
            SEL selector = NSSelectorFromString(@"didAddedQuestion:");
            if ([self.delegate respondsToSelector:selector]) {
                [self.delegate performSelectorOnMainThread:selector withObject:dataDicc waitUntilDone:YES];
            }
        }
        else{
            [self didFailedAddingQuestion:[dataDicc objectForKey:@"message"]];
        }
    }
    else{
        [self didFailedAddingQuestion:@"Error parsing the JSON"];
    }
}

-(void)didFailedAddingQuestion:(NSString *)errorMessage{
    SEL selector = NSSelectorFromString(@"didFailedAddingQuestion:");
    if ([self.delegate respondsToSelector:selector]) {
        [self.delegate performSelectorOnMainThread:selector withObject:errorMessage waitUntilDone:YES];
    }
}








@end
