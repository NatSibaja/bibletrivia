//
//  BTQuestionDelegate.h
//  BibleTrivia
//
//  Created by Natalia Sibaja on 2/21/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol BTQuestionDelegate <NSObject>

-(void)didReceiveQuestion:(NSString *)data;
-(void)didReceiveQuestionError:(NSString *)error;

@optional

-(void)didAddedQuestion:(NSDictionary *)data;
-(void)didFailedAddingQuestion:(NSString *)errorMessage;

@end
