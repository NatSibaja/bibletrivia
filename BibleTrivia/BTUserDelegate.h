//
//  BTUserDelegate.h
//  BibleTrivia
//
//  Created by Natalia Sibaja on 3/17/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol BTUserDelegate <NSObject>

@optional

-(void)didReceiveUser:(User *)user;
-(void)didReceiveUserFailed:(NSString *)errorMessage;

-(void)didUpdateUser:(NSDictionary *)userData;
-(void)didUpdateUserError:(NSString *)errorMessage;

-(void)didAddedUser:(NSDictionary *)userData;
-(void)didAddedUserError:(NSString *)errorMessage;

-(void)didReceiveUserStats:(NSDictionary *)data;
-(void)didReceiveUserStatsError:(NSString *)errorMessage;
@end