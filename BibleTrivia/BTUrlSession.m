//
//  IBUrlSession.m
//  iBelieve
//
//  Created by Leonardo Ortiz on 11/14/13.
//  Copyright (c) 2013 Salem. All rights reserved.
//

#import "BTUrlSession.h"
#import "Reachability.h"

#import <SystemConfiguration/SystemConfiguration.h>


#define NETWORK_ERROR_MESSAGE   @"Error making the request to the server"
#define PARSING_ERROR_MESSAGE   @"Error pasing the data into JSON"
#define NO_NERWORK_MESSAGE      @"Error no network"
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"



@interface BTUrlSession()<NSURLSessionDelegate, NSURLSessionTaskDelegate>

-(NSString *) paramsToString:(NSMutableDictionary *)params;
-(void)debug:(NSString *)message;
-(void)didReceiveResponse:(NSData *)data;
-(void)didReceiveError:(NSUrlErrorType)error;
-(BOOL)isNetworkAvailable;
-(void)doRequest:(NSString *)stringUrl
      withParams:(NSMutableDictionary*)params
      andHeaders:(NSDictionary *)headers
withParamsFormat:(ParamFormat)paramsFormat
    andOperation:(NSString *)operation;


@end

@implementation BTUrlSession
#pragma mark -
#pragma mark Post Request Methods

-(void)doPostRequest:(NSString *)stringUrl
          withParams:(NSMutableDictionary *)params{
    NSDictionary * headers = [[NSDictionary alloc]initWithObjectsAndKeys:@"Content-Type",@"application/json", nil];
    [self doPostRequest:stringUrl withParams:params andHeaders:headers withParamsFormat:KeyValue];
    
}

-(void)doPostRequest:(NSString *)stringUrl
          withParams:(NSMutableDictionary *)params
          andHeaders:(NSDictionary *)headers{
    [self doPostRequest:stringUrl withParams:params andHeaders:headers withParamsFormat:KeyValue];
}

-(void)doPostRequest:(NSString *)stringUrl
          withParams:(NSMutableDictionary *)params
          andHeaders:(NSDictionary *)headers
    withParamsFormat:(ParamFormat)paramsFormat{
    [self doRequest:stringUrl withParams:params andHeaders:headers withParamsFormat:paramsFormat andOperation:@"POST"];
}


#pragma mark -
#pragma mark Get methods

-(void)doGetRequest:(NSString *)stringUrl
          withParams:(NSMutableDictionary *)params{
    NSDictionary * headers = [[NSDictionary alloc]initWithObjectsAndKeys:@"Content-Type",@"application/json", nil];
    [self doGetRequest:stringUrl withParams:params andHeaders:headers];
    
}

-(void)doGetRequest:(NSString *)stringUrl
         withParams:(NSMutableDictionary *)params
         andHeaders:(NSDictionary *)headers{
    [self doRequest:stringUrl withParams:params andHeaders:headers withParamsFormat:KeyValue andOperation:@"GET"];
}

#pragma mark -
#pragma mark Delete methods

-(void)doDeleteRequest:(NSString *)stringUrl withParams:(NSMutableDictionary *)params{
    NSDictionary * headers = [[NSDictionary alloc]initWithObjectsAndKeys:@"Content-Type",@"application/json", nil];
    [self doRequest:stringUrl withParams:params andHeaders:headers withParamsFormat:KeyValue andOperation:@"DELETE"];
}

-(void)doDeleteRequest:(NSString *)stringUrl
            withParams:(NSMutableDictionary *)params
            andHeaders:(NSDictionary *)headers{
    [self doRequest:stringUrl withParams:params andHeaders:headers withParamsFormat:KeyValue andOperation:@"DELETE"];
}

#pragma mark -
#pragma mark Put methods

-(void)doPutRequest:(NSString *)stringUrl withParams:(NSMutableDictionary *)params{
    NSDictionary * headers = [[NSDictionary alloc]initWithObjectsAndKeys:@"Content-Type",@"application/json", nil];
    [self doRequest:stringUrl withParams:params andHeaders:headers withParamsFormat:KeyValue andOperation:@"PUT"];
}

-(void)doPutRequest:(NSString *)stringUrl
            withParams:(NSMutableDictionary *)params
            andHeaders:(NSDictionary *)headers{
    [self doRequest:stringUrl withParams:params andHeaders:headers withParamsFormat:JsonObject andOperation:@"PUT"];
}

#pragma mark -
#pragma mark General request

-(void)doRequest:(NSString *)stringUrl
      withParams:(NSMutableDictionary*)params
      andHeaders:(NSDictionary *)headers
withParamsFormat:(ParamFormat)paramsFormat
    andOperation:(NSString *)operation{
    if ([self isNetworkAvailable]) {
        NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
        sessionConfiguration.HTTPAdditionalHeaders = headers;
        NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration];
        NSURL *url = [NSURL URLWithString:stringUrl];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [request setValue:[headers valueForKey:@"Content-Type"] forHTTPHeaderField:@"Content-Type"];
        request.HTTPBody = [[self paramsToString:params withFormat:paramsFormat] dataUsingEncoding:NSUTF8StringEncoding];
        request.HTTPMethod = operation;
        NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
            if (error != nil) {
                [self debug:NETWORK_ERROR_MESSAGE];
                [self didReceiveError:UNREACHABLE_NETWORK];
            }
            else{
                [self didReceiveResponse:data];
            }
            
        }];
        [postDataTask resume];
    }
    else{
        [self didReceiveError:NO_NETWORK_ERROR];
    }
    
}
- (void)URLSession:(NSURLSession *)session didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition, NSURLCredential *))completionHandler{
    if([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]){
        if([challenge.protectionSpace.host isEqualToString:API_URL]){
            NSURLCredential *credential = [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust];
            completionHandler(NSURLSessionAuthChallengeUseCredential,credential);
        }
    }
}
-(void)didReceiveResponse:(NSData *)data{
    if ([self.delegate respondsToSelector:self.didReceiveData]) {
        [self.delegate performSelector:self.didReceiveData withObject:data];
    }
}

-(void)didReceiveError:(NSUrlErrorType)error{
    if (DEBUG_ENABLED&&DEBUG_URLRESQUESTS) {
        switch (error) {
            case UNREACHABLE_NETWORK:
                [self debug:NETWORK_ERROR_MESSAGE];
                break;
            case NO_NETWORK_ERROR:
                [self debug:NO_NERWORK_MESSAGE];
            default:
                break;
        }
    }
    if ([self.delegate respondsToSelector:self.didReceiveError]) {
        [self.delegate performSelector:self.didReceiveError withObject:[NSNumber numberWithInt:error]];
    }
    
}

#pragma mark -
#pragma mark Util methods

-(NSString *)paramsToString:(NSMutableDictionary *)params{
    return [self paramsToString:params withFormat:KeyValue];
}

-(NSString *)paramsToString:(NSMutableDictionary *)params withFormat:(ParamFormat)format{
    if (params==nil) {
        return @"";
    }
    else{
        if (format==KeyValue) {
            NSMutableString *stringParams = [[NSMutableString alloc]init];
            [params enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop){
                [stringParams appendString:[NSString stringWithFormat:@"%@=%@&",key,obj]];
            }];
            [stringParams deleteCharactersInRange:NSMakeRange([stringParams length]-1, 1)];
            [self debug:stringParams];
            return stringParams;
        }
        else if (format==JsonObject){
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:params options:kNilOptions error:nil];
            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            return jsonString;
        }
        else{
            [self debug:@"Unknow param format"];
            return @"";
        }

    }
}




-(void)debug:(NSString *)message{
    if (DEBUG_ENABLED&&DEBUG_URLRESQUESTS) {
        NSLog(@"%@",message);
    }
}

-(BOOL)isNetworkAvailable{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return !(networkStatus == NotReachable);
}


@end
