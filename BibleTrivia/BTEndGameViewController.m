//
//  BTEndGameViewController.m
//  BibleTrivia
//
//  Created by Natalia Sibaja on 3/3/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import "BTEndGameViewController.h"
#import "KAProgressLabel.h"
#import "BTSocial.h"
#import "BTUserDataSource.h"
#import "User+BTUser.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "BTAppDelegate.h"

@interface BTEndGameViewController ()

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *resultPadding;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *percentPadding;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *playHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *faceHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tweetHeight;
@property (nonatomic) IBOutlet UIBarButtonItem* revealButtonItem;

@property (strong, nonatomic) IBOutlet KAProgressLabel *scoreResult;
@property (weak, nonatomic) IBOutlet UILabel *resultTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *resultTextLabel;
@property (weak, nonatomic) IBOutlet UIButton *shareButton;
@property (weak, nonatomic) IBOutlet UIButton *tweetButton;


- (IBAction)shareTouched:(id)sender;
- (IBAction)tweetTouched:(id)sender;
@end

@implementation BTEndGameViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupView];
    [self setupSideBar];
    [self drawProgressCircle];
}
-(void)viewWillAppear:(BOOL)animated
{
    self.screenName = @"End Game Screen";
    [Flurry logEvent:@"End Game Screen"];
}
-(void)setupSideBar
{
    [self.revealButtonItem setTarget: self.revealViewController];
    [self.revealButtonItem setAction: @selector(revealToggle:)];
    [self.navigationController.navigationBar addGestureRecognizer: self.revealViewController.panGestureRecognizer];
}
-(void)drawProgressCircle
{
    int totalQuestions = NO_QUESTIONS;
    float percentile = (self.trivia.score.floatValue / (float)totalQuestions)*100;
    [self.scoreResult setBorderWidth: 9];
    [self.scoreResult setColorTable: @{
                                       NSStringFromProgressLabelColorTableKey(ProgressLabelTrackColor):
                                           [UIColor colorWithRed:0.353 green:0.808 blue:0.996 alpha:1],
                                       NSStringFromProgressLabelColorTableKey(ProgressLabelProgressColor):[UIColor whiteColor]}];
    [self.scoreResult setStartDegree:0.0];
    [self.scoreResult setClockWise:YES];
    float progress = percentile/100;
    [self.scoreResult setProgress: progress
                     timing:TPPropertyAnimationTimingEaseOut
                   duration:1.5
                      delay:0.5];
    [self.scoreResult setTextColor:[UIColor whiteColor]];
    [self.scoreResult setText:[NSString stringWithFormat:@"%i",(int)percentile]];
    if (percentile > 69) {
        [self.resultTitleLabel setText:@"Congratulations!"];
    }
    else{
        [self.resultTitleLabel setText:@"Keep on trying"];
    }
    if (percentile == 100) {
        [self.scoreResult setFont:[UIFont systemFontOfSize:25]];
    }
    if (self.trivia.score == nil) {
        self.trivia.score = [NSNumber numberWithInt:0];
    }
    [self.resultTextLabel setText:[NSString stringWithFormat: @"You are a %@.\nTell your friends the level of your Biblical knowledge!", [self scoreLevelText:percentile]]];
    
    Level player = percentile;
    NSLog(@"%u",player);
}
-(NSString *)scoreLevelText:(int)score
{
    switch (score) {
        case 0 ... 20:
            return @"Church Goer";
            break;
        case 21 ... 40:
            return @"Seminary Graduate";
            break;
        case 41 ... 60:
            return @"Bible College Student";
            break;
        case 61 ... 80:
            return @"Theologian";
            break;
        case 81 ... 100:
            return @"Top Biblical Scholar";
            break;
        default:
            return @"";
            break;
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setupView
{
    if(!isiPhone5)
    {
        self.resultPadding.constant = 15;
        self.percentPadding.constant = 50;
        self.playHeight.constant = 60;
        self.faceHeight.constant = 60;
        self.tweetHeight.constant = 60;
    }
    self.navigationItem.hidesBackButton = YES;
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]];
    BTUserDataSource *datasource = [[BTUserDataSource alloc]init];
    datasource.delegate = self;
    User *user = [datasource getUserFromContext];
    if (user == nil){
        self.shareButton.enabled = NO;
        self.tweetButton.enabled = NO;
    }
}
- (IBAction)shareTouched:(id)sender {
    BTSocial *sharing = [[BTSocial alloc]init];
    sharing.rootView = self;
    NSString *scores =[NSString stringWithFormat: NSLocalizedString(@"FB_MESSAGE", @""), self.scoreResult.text];
    [sharing shareToFacebook:scores andImage:[UIImage imageNamed:SHARE_IMAGE] andUrl:[NSURL URLWithString:FB_SHARE_URL]];
}

- (IBAction)tweetTouched:(id)sender {
    BTSocial *sharing = [[BTSocial alloc]init];
    sharing.rootView = self;
    NSString *scores =[NSString stringWithFormat: NSLocalizedString(@"TWITTER_MESSAGE", @""), self.scoreResult.text];
    [sharing shareToTwitter:scores andImage:[UIImage imageNamed:SHARE_IMAGE] andUrl:[NSURL URLWithString:TWITTER_SHARE_URL]];
}
@end
