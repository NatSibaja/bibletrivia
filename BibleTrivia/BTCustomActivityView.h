//
//  BTCustomActivityView.h
//  BibleTrivia
//
//  Created by Natalia Sibaja on 3/19/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BTCustomActivityView : UIView
@property (strong, nonatomic) IBOutlet UIView *content;
@property (strong, nonatomic) IBOutlet UIImageView *activityImageView;

-(void)startAnimating;
-(void)stopAnimating;

@end
