//
//  Score.h
//  BibleTrivia
//
//  Created by Leonardo Ortiz on 4/9/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class User;

@interface Score : NSManagedObject

@property (nonatomic, retain) NSString * correct;
@property (nonatomic, retain) NSString * percentCorrect;
@property (nonatomic, retain) NSNumber * questions;
@property (nonatomic, retain) NSNumber * userId;
@property (nonatomic, retain) NSString * userName;
@property (nonatomic, retain) User *userInfo;

@end
