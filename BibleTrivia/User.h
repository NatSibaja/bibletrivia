//
//  User.h
//  TriviaGame
//
//  Created by Natalia Sibaja on 6/4/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Application, Score;

@interface User : NSManagedObject

@property (nonatomic, retain) NSString * accessToken;
@property (nonatomic, retain) NSString * authenticationKey;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSNumber * facebookId;
@property (nonatomic, retain) NSString * firstName;
@property (nonatomic, retain) NSString * lastName;
@property (nonatomic, retain) NSString * password;
@property (nonatomic, retain) NSString * postalCode;
@property (nonatomic, retain) NSNumber * userId;
@property (nonatomic, retain) NSString * userScore;
@property (nonatomic, retain) NSNumber * subscribe;
@property (nonatomic, retain) Application *application;
@property (nonatomic, retain) Score *userScores;

@end
