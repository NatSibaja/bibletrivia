
//
//  BTUserDataSource.m
//  BibleTrivia
//
//  Created by Natalia Sibaja on 2/25/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import "BTUserDataSource.h"
#import "User+BTUser.h"
#import "BTManagedObjectContext.h"
#import "BTUrlSession.h"
#import "NSData+Additions.h"



@interface BTUserDataSource()
-(void)didReceiveUserStatsError:(NSString *)errorMessage;
@end

@implementation BTUserDataSource

-(void)saveInContext:(User *)user{
    [user save];
}
-(void)eraseFromContext:(User *)user{
    [user erase];
}

-(User *)getUserFromContext{
    NSFetchRequest * request = [[NSFetchRequest alloc]init];
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"User"
                                                         inManagedObjectContext:[BTManagedObjectContext sharedInstance].managedObjectContext];
    [request setEntity:entityDescription];
    NSError *error=nil;
    NSArray * mutableFetchResults = [[BTManagedObjectContext sharedInstance].managedObjectContext executeFetchRequest:request error:&error];
    if (mutableFetchResults == nil) {
        if (DEBUG_COREDATA&&DEBUG_ENABLED) {
            NSLog(@"CoreData:Error selecting the user");
        }
        return nil;
    }
    else{
        if ([mutableFetchResults count]>0) {
            return [mutableFetchResults objectAtIndex:0];
        }
        else{
            return nil;
        }
    }
}

-(User *)insertUserInContext{
    User *user = nil;
    user = [NSEntityDescription insertNewObjectForEntityForName:@"User"
                                         inManagedObjectContext:[BTManagedObjectContext sharedInstance].managedObjectContext];
    return user;
}

#pragma mark -
#pragma mark - Server methods

#pragma mark - Get user
-(void)getUserFromServer:(NSString *)authKey{
    BTUrlSession *btUrlSession = [[BTUrlSession alloc]init];
    btUrlSession.delegate=self;
    btUrlSession.didReceiveData = NSSelectorFromString(@"didReceiveGetACK:");
    btUrlSession.didReceiveError = NSSelectorFromString(@"didReceiveGetACKError:");
    NSString *stringUrl = [NSString stringWithFormat:@"%@%@",API_URL,USER_URL];
    NSString *authStr = [NSString stringWithFormat:@"%@:%@", API_KEY,authKey];
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", [authData base64Encoding]];    [btUrlSession doGetRequest:stringUrl withParams:nil andHeaders:@{@"Content-Type":@"application/json",@"Authorization":authValue}];
}

-(void)didReceiveGetACK:(NSData *)data{
    NSError *error;
    NSDictionary *dataDicc = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    NSString *message = [dataDicc objectForKey:@"Message"];
    if (message!=nil) {
        [self didReceiveGetACKError:message];
    }
    else{
        dataDicc = [dataDicc objectForKey:@"item"];
        User * user = [self getUserFromContext];
        if (user==nil) {
            user = [self insertUserInContext];
        }
        user.email = [dataDicc objectForKey:@"email"];
        user.firstName = [dataDicc objectForKey:@"firstName"];
        user.lastName = [dataDicc objectForKey:@"lastName"];
        user.userId = [NSNumber numberWithInt:[[dataDicc objectForKey:@"userid"] intValue]];
        SEL selector = NSSelectorFromString(@"didReceiveUser:");
        if ([self.delegate respondsToSelector:selector]) {
            [self.delegate performSelectorOnMainThread:selector withObject:user waitUntilDone:YES];
        }
    }
}

-(void)didReceiveGetACKError:(NSString *)error{
    SEL selector = NSSelectorFromString(@"didReceiveUserFailed:");
    if ([self.delegate respondsToSelector:selector]) {
        [self.delegate performSelectorOnMainThread:selector withObject:error waitUntilDone:YES];
    }
    
}

#pragma mark -
#pragma mark Update user to the server

-(void)updateUserIntoServer:(NSMutableDictionary *)params andUser:(User *)user{
    BTUrlSession *btUrlSession = [[BTUrlSession alloc]init];
    btUrlSession.delegate=self;
    btUrlSession.didReceiveData = NSSelectorFromString(@"didUpdateACK:");
    btUrlSession.didReceiveError = NSSelectorFromString(@"didUpdateACKError:");
    NSString *stringUrl = [NSString stringWithFormat:@"%@%@",API_URL,USER_URL];
    NSString *authStr = [NSString stringWithFormat:@"%@:%@", API_KEY,user.authenticationKey];
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", [authData base64Encoding]];
    [btUrlSession doPutRequest:stringUrl withParams:params andHeaders:@{@"Content-Type":@"application/json",@"Authorization":authValue} ];
}
-(void)didUpdateACK:(NSData *)data{
    NSError *error;
    NSDictionary *dataDicc = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    NSString *message = [dataDicc objectForKey:@"messages"];
    if (message == nil) {
        dataDicc = [dataDicc objectForKey:@"item"];
        SEL selector = NSSelectorFromString(@"didUpdateUser:");
        if ([self.delegate respondsToSelector:selector]) {
            [self.delegate performSelectorOnMainThread:selector withObject:dataDicc waitUntilDone:YES];
        }
    }
    else{
        [self didUpdateACKError:message];
    }
}

-(void)didUpdateACKError:(NSString *)errorMessage{
    SEL selector = NSSelectorFromString(@"didUpdateUserError:");
    if ([self.delegate respondsToSelector:selector]) {
        [self.delegate performSelectorOnMainThread:selector withObject:errorMessage waitUntilDone:YES];
    }
}

#pragma mark -
#pragma mark Add user

-(void)addUserIntoServer:(User *)user{
    BTUrlSession *btUrlSession = [[BTUrlSession alloc]init];
    btUrlSession.delegate=self;
    btUrlSession.didReceiveData = NSSelectorFromString(@"didAddedACK:");
    btUrlSession.didReceiveError = NSSelectorFromString(@"didAddedACKError:");
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:(user.email == nil)?@"":user.email forKey:@"email"];
    [params setObject:(user.password == nil)?@"":user.password forKey:@"password"];
    [params setObject:(user.firstName == nil)?@"":user.firstName forKey:@"firstName"];
    [params setObject:(user.lastName == nil)?@"":user.lastName forKey:@"lastName"];
    [params setObject:(user.postalCode == nil)?@"":user.postalCode forKey:@"postalCode"];
    //[params setObject:(user.facebookId == nil)?@"":user.facebookId forKey:@"facebookId"];
    //[params setObject:(user.accessToken == nil)?@"":user.accessToken forKey:@"accessToken"];
    NSString *stringUrl = [NSString stringWithFormat:@"%@%@",API_URL,USER_URL];
    NSString *authStr = [NSString stringWithFormat:@"%@:", API_KEY];
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", [authData base64Encoding]];
    [btUrlSession doPostRequest:stringUrl withParams:params andHeaders:@{@"Content-Type":@"application/json",@"Authorization":authValue} withParamsFormat:JsonObject];
}

-(void)didAddedACK:(NSData *)data{
    NSError *error;
    NSDictionary *dataDicc = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    NSString *message = [[dataDicc objectForKey:@"resultType"]objectForKey:@"message"];
    if ([message isEqualToString:@"Success"]) {
        dataDicc = [dataDicc objectForKey:@"data"];
        SEL selector = NSSelectorFromString(@"didAddedUser:");
        if ([self.delegate respondsToSelector:selector]) {
            [self.delegate performSelectorOnMainThread:selector withObject:dataDicc waitUntilDone:YES];
        }
    }
    else{
        [self didAddedACKError:message];
    }
}

-(void)didAddedACKError:(NSString *)errorMessage{
    SEL selector = NSSelectorFromString(@"didAddedUserError:");
    if ([self.delegate respondsToSelector:selector]) {
        [self.delegate performSelectorOnMainThread:selector withObject:errorMessage waitUntilDone:YES];
    }
}
#pragma mark -
#pragma mark subscribe user to newsletter
-(void)subscribeNewsletter:(User *)user
{
    BTUrlSession *btUrlSession = [[BTUrlSession alloc]init];
    btUrlSession.delegate=self;
    btUrlSession.didReceiveData = NSSelectorFromString(@"didSubscribe:");
    btUrlSession.didReceiveError = NSSelectorFromString(@"didSubscribeError:");
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:(user.email == nil)?@"":user.email forKey:@"EmailAddress"];
    [params setObject:(user.firstName == nil)?@"":user.firstName forKey:@"FirstName"];
    [params setObject:(user.lastName == nil)?@"":user.lastName forKey:@"LastName"];
    [params setObject:(user.postalCode == nil)?@"":user.postalCode forKey:@"PostalCode"];
    NSString *stringUrl = [NSString stringWithFormat:@"%@%@",API_URL,NEWSLETTER_URL];
    NSString *authStr = [NSString stringWithFormat:@"%@:", API_KEY];
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", [authData base64Encoding]];
    [btUrlSession doPostRequest:stringUrl withParams:params andHeaders:@{@"Content-Type":@"application/json",@"Authorization":authValue} withParamsFormat:JsonObject];
}
-(void)didSubscribe:(NSData *)data{
    NSError *error;
    NSDictionary *dataDicc = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    NSString *message = [[dataDicc objectForKey:@"resultType"]objectForKey:@"message"];
    if ([message isEqualToString:@"Success"]) {
        dataDicc = [dataDicc objectForKey:@"data"];
        SEL selector = NSSelectorFromString(@"didSubscribeUser:");
        if ([self.delegate respondsToSelector:selector]) {
            [self.delegate performSelectorOnMainThread:selector withObject:dataDicc waitUntilDone:YES];
        }
    }
    else{
        [self didSubscribeError:message];
    }
}

-(void)didSubscribeError:(NSString *)errorMessage{
    SEL selector = NSSelectorFromString(@"didSubscribeUserError:");
    if ([self.delegate respondsToSelector:selector]) {
        [self.delegate performSelectorOnMainThread:selector withObject:errorMessage waitUntilDone:YES];
    }
}
#pragma mark -
#pragma mark get user stats

-(void)getUserStats:(User *)user{
    int userId = [user.userId intValue];
    BTUrlSession *urlSession = [[BTUrlSession alloc]init];
    urlSession.delegate = self;
    urlSession.didReceiveData = NSSelectorFromString(@"didReceiveUserStats:");
    urlSession.didReceiveError = NSSelectorFromString(@"didReceiveUserStatsError:");
    NSString *stringUrl = [NSString stringWithFormat:@"%@%@/%i",API_URL,USER_STATS_URL,userId];
    NSString *authStr = [NSString stringWithFormat:@"%@:%@", API_KEY,user.authenticationKey];
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", [authData base64Encoding]];
    [urlSession doGetRequest:stringUrl withParams:nil andHeaders:@{@"Content-Type":@"application/json",@"Authorization":authValue}];
}

-(void)didReceiveUserStats:(NSData *)data{
    NSError *error;
    NSDictionary *dataDicc = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    if (error==nil) {
        NSString *message = [[dataDicc objectForKey:@"resultType"]objectForKey:@"message"];
        if ([message isEqualToString:@"Success"]) {
            SEL selector = NSSelectorFromString(@"didReceiveUserStats:");
            if ([self.delegate respondsToSelector:selector]) {
                [self.delegate performSelectorOnMainThread:selector withObject:dataDicc waitUntilDone:YES];
            }
        }
        else{
            [self didReceiveUserStatsError:[dataDicc objectForKey:@"message"]];
        }
    }
    else{
        [self didReceiveUserStatsError:@"Error parsing the JSON"];
    }
    
}

-(void)didReceiveUserStatsError:(NSString *)errorMessage{
    SEL selector = NSSelectorFromString(@"didReceiveUserStatsError:");
    if ([self.delegate respondsToSelector:selector]) {
        [self.delegate performSelectorOnMainThread:selector withObject:errorMessage waitUntilDone:YES];
    }
}






@end
