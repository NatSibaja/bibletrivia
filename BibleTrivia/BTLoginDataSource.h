//
//  BTLoginDataSource.h
//  BibleTrivia
//
//  Created by Natalia Sibaja on 2/25/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BTLoginDataSource : NSObject
@property (nonatomic,strong) id delegate;
//Native login
-(void)login:(NSString *)email andPassword:(NSString *)password;

//FB login
-(void)login:(NSString *)firstName :(NSString *)lastName :(NSString *)email :(NSString *)facebookId :(NSString *)accessToken;
@end
