//
//  BTScoresViewController.m
//  BibleTrivia
//
//  Created by Natalia Sibaja on 3/17/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import "BTScoresViewController.h"
#import "KAProgressLabel.h"
#import "BTUserDataSource.h"
#import "BTUserDelegate.h"
#import "BTSocial.h"
#import "User.h"
#import "BTTriviaDataSource.h"
#import "Trivia+BTTrivia.h"
#import "Trivia.h"
#import "BTCustomActivityView.h"

@interface BTScoresViewController ()<BTUserDelegate,UIAlertViewDelegate>
@property (nonatomic) IBOutlet UIBarButtonItem* revealButtonItem;
@property (weak, nonatomic) IBOutlet UIView *fadeView;
@property (weak, nonatomic) IBOutlet KAProgressLabel *bestScoreLabel;
@property (weak, nonatomic) IBOutlet KAProgressLabel *lifetimeScoreLabel;
@property (weak, nonatomic) IBOutlet BTCustomActivityView *activityView;

@property (strong, nonatomic) Trivia *triviaGame;
@property (nonatomic, assign) BOOL bestScoreReset;
@property (nonatomic, assign) BOOL lifetimeScoreReset;

- (IBAction)shareTouched:(id)sender;
- (IBAction)tweetTouched:(id)sender;
- (IBAction)resetScore:(id)sender;

@end

@implementation BTScoresViewController
@synthesize bestScoreLabel;
@synthesize lifetimeScoreLabel;
@synthesize bestScoreReset, lifetimeScoreReset;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.activityView startAnimating];
    [self setupView];
    [self setupSideBar];
    [self setScores];
}
-(void)viewWillAppear:(BOOL)animated
{
    self.screenName = @"My Scores";
    [Flurry logEvent:@"My Scores"];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setupView
{
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]]];
    [self.fadeView setBackgroundColor:BLUE_FADE];
    self.bestScoreReset = NO;
    self.lifetimeScoreReset = NO;
}

-(void)setupSideBar
{
    [self.revealButtonItem setTarget: self.revealViewController];
    [self.revealButtonItem setAction: @selector( revealToggle: )];
    [self.navigationController.navigationBar addGestureRecognizer: self.revealViewController.panGestureRecognizer];
}
-(void)setScores
{
    BTTriviaDataSource *triviaSource = [[BTTriviaDataSource alloc]init];
    self.triviaGame = [triviaSource getTriviaFromContext];

    BTUserDataSource *datasource = [[BTUserDataSource alloc]init];
    datasource.delegate = self;
    
    User *user = [datasource getUserFromContext];
    [datasource getUserStats:user];
}
-(void)didReceiveUserStats:(NSDictionary *)data
{
    [self drawProgressCircle:self.bestScoreLabel :self.triviaGame.bestRound.floatValue];
    [self drawProgressCircle:self.lifetimeScoreLabel:[[[data objectForKey:@"data"] objectForKey:@"percentCorrect"] floatValue]];
    [self.activityView stopAnimating];
}
-(void)didReceiveUserStatsError:(NSString *)errorMessage
{
    NSLog(@"Error getting stats");
}
-(void)drawProgressCircle:(KAProgressLabel *)scorelabel :(float)score
{
    [scorelabel setColorTable: @{
                                       NSStringFromProgressLabelColorTableKey(ProgressLabelTrackColor):
                                           [UIColor colorWithRed:0.353 green:0.808 blue:0.996 alpha:1],
                                       NSStringFromProgressLabelColorTableKey(ProgressLabelProgressColor):[UIColor whiteColor]}];
    [scorelabel setStartDegree:0.0];
    [scorelabel setBorderWidth: 9];
    [scorelabel setClockWise:YES];
    float progress = score/100;
    [scorelabel setProgress: progress
                     timing:TPPropertyAnimationTimingEaseOut
                   duration:1.5
                      delay:0.5];
    [scorelabel setTextColor:[UIColor whiteColor]];
    [scorelabel setText:[NSString stringWithFormat:@"%i",(int)score]];
  
}
- (IBAction)shareTouched:(id)sender {
    BTSocial *sharing = [[BTSocial alloc]init];
    sharing.rootView = self;
    NSString *scores =[NSString stringWithFormat: NSLocalizedString(@"FB_MESSAGE", @""), self.lifetimeScoreLabel.text];
    [sharing shareToFacebook:scores andImage:[UIImage imageNamed:SHARE_IMAGE] andUrl:[NSURL URLWithString:FB_SHARE_URL]];
}

- (IBAction)tweetTouched:(id)sender {
    BTSocial *sharing = [[BTSocial alloc]init];
    sharing.rootView = self;
    NSString *scores =[NSString stringWithFormat: NSLocalizedString(@"TWITTER_MESSAGE", @""), self.lifetimeScoreLabel.text];
    [sharing shareToTwitter:scores andImage:[UIImage imageNamed:SHARE_IMAGE] andUrl:[NSURL URLWithString:TWITTER_SHARE_URL]];
}
- (IBAction)resetScore:(id)sender
{
    
    UIButton *button = (UIButton *)sender;
    if (button.tag == 1)
    {
        self.bestScoreReset = YES;
    }
    else if (button.tag == 2)
    {
        self.lifetimeScoreReset = YES;
    }
    [self showMessage:NSLocalizedString(@"RESET_MESSAGE",@"") withTitle:NSLocalizedString(@"ALERT",@"")];
}
- (void)showMessage:(NSString *)text withTitle:(NSString *)title
{
    [[[UIAlertView alloc] initWithTitle:title
                                message:text
                               delegate:self
                      cancelButtonTitle:NSLocalizedString(@"CANCEL_BUTTON", @"")
                      otherButtonTitles:NSLocalizedString(@"OK_BUTTON", @""),nil] show];
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        if (self.bestScoreReset) {
            [self drawProgressCircle:self.bestScoreLabel:0];
            self.bestScoreReset = NO;
        }
        else if (self.lifetimeScoreReset){
            [self drawProgressCircle:self.lifetimeScoreLabel :0];
            self.lifetimeScoreReset = NO;
        }
    }
}
@end
