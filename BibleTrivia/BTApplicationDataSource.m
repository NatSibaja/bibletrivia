//
//  BTApplicationDataSource.m
//  BibleTrivia
//
//  Created by Leonardo Ortiz on 3/24/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import "BTApplicationDataSource.h"
#import "Application.h"
#import "Application+BTApplication.h"
#import "BTManagedObjectContext.h"

@interface BTApplicationDataSource()
-(Application *)getApplicationFromContext;
@end
@implementation BTApplicationDataSource

-(Application *)getApplication{
    Application *app = [self getApplicationFromContext];
    if (app==nil) {
        app = [NSEntityDescription insertNewObjectForEntityForName:@"Application"
                                             inManagedObjectContext:[BTManagedObjectContext sharedInstance].managedObjectContext];
        app.books=nil;
        [app save];
    }
    return app;
}



-(Application *)getApplicationFromContext{
    NSFetchRequest * request = [[NSFetchRequest alloc]init];
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"Application"
                                                         inManagedObjectContext:[BTManagedObjectContext sharedInstance].managedObjectContext];
    [request setEntity:entityDescription];
    NSError *error=nil;
    NSArray * mutableFetchResults = [[BTManagedObjectContext sharedInstance].managedObjectContext executeFetchRequest:request error:&error];
    if (mutableFetchResults == nil) {
        if (DEBUG_COREDATA&&DEBUG_ENABLED) {
            NSLog(@"CoreData:Error selecting the user");
        }
        return nil;
    }
    else{
        if ([mutableFetchResults count]>0) {
            return [mutableFetchResults objectAtIndex:0];
        }
        else{
            return nil;
        }
    }
}

@end
