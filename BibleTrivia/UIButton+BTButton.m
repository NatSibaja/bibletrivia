//
//  UIButton+BTButton.m
//  BibleTrivia
//
//  Created by Natalia Sibaja on 3/14/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import "UIButton+BTButton.h"

@implementation UIButton (BTButton)

-(void)fillGradientBackground:(UIColor* )startColor :(UIColor *)endColor
{
    CAGradientLayer *gradient = [CAGradientLayer layer];
    CALayer *layer = self.layer;
    gradient.frame = layer.bounds;
    gradient.colors = [NSArray arrayWithObjects:(id)[startColor CGColor], (id)[endColor CGColor], nil];
    [layer insertSublayer:gradient atIndex:0];
    [self bringSubviewToFront:self.imageView];
}

@end
