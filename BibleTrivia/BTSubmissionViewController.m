//
//  BTSubmissionViewController.m
//  BibleTrivia
//
//  Created by Natalia Sibaja on 2/25/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import "BTSubmissionViewController.h"
#import "UIButton+BTButton.h"
#import "BTQuestionDataSource.h"
#import "BTQuestionDelegate.h"
#import "BTPickerView.h"
#import "BTCustomActivityView.h"

@interface BTSubmissionViewController ()<UITextFieldDelegate, BTQuestionDelegate, UITextViewDelegate, UIAlertViewDelegate>

@property (nonatomic) IBOutlet UIBarButtonItem* revealButtonItem;
@property (weak, nonatomic) IBOutlet UITextView *questionText;
@property (weak, nonatomic) IBOutlet UITextField *answer1;
@property (weak, nonatomic) IBOutlet UITextField *answer2;
@property (weak, nonatomic) IBOutlet UITextField *answer3;
@property (weak, nonatomic) IBOutlet UITextField *answer4;
@property (weak, nonatomic) IBOutlet UISwitch *switch1;
@property (weak, nonatomic) IBOutlet UISwitch *switch2;
@property (weak, nonatomic) IBOutlet UISwitch *switch3;
@property (weak, nonatomic) IBOutlet UISwitch *switch4;
@property (weak, nonatomic) IBOutlet UITextField *bibleReference;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *fadeView;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (weak, nonatomic) IBOutlet BTCustomActivityView *activityView;
@property (strong, nonatomic) BTPickerView *pickerView;
@property (strong, nonatomic) NSMutableArray *bookArray;
@property (strong, nonatomic) NSMutableArray *bookIdArray;
@property (strong, nonatomic) NSMutableArray *chapterArray;
@property (strong, nonatomic) NSMutableArray *chapterCount;
@property (strong, nonatomic) NSMutableArray *verseArray;
@property (strong, nonatomic) BTQuestion *question;
@property (strong, nonatomic) NSString *book, *chapter, *start, *end;
@property bool success;

- (IBAction)submitTouched:(id)sender;
@end

@implementation BTSubmissionViewController
@synthesize bookArray;
@synthesize bookIdArray;
@synthesize chapterArray;
@synthesize chapterCount;
@synthesize verseArray;
@synthesize pickerView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupView];
    [self setupSideBar];
    [self setupTextFieldPlaceHolders];
    svos = self.scrollView.contentOffset;
    [self setupTextFields];
}
-(void)viewDidAppear:(BOOL)animated
{
    self.book = [self.bookArray objectAtIndex:0];
    self.chapter= @"1";
    self.start= @"1";
    self.end = @"1";
}
-(void)viewWillAppear:(BOOL)animated
{
    self.screenName = @"Add a question";
    [Flurry logEvent:@"Add a question"];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setupView
{
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]]];
    [self.fadeView setBackgroundColor:BLUE_FADE];
    [self.submitButton fillGradientBackground:DARK_GREEN :LIGHT_GREEN];
}
-(void)setupSideBar
{
    [self.revealButtonItem setTarget: self.revealViewController];
    [self.revealButtonItem setAction: @selector( revealToggle: )];
    [self.navigationController.navigationBar addGestureRecognizer: self.revealViewController.panGestureRecognizer];
}

-(void)setupTextFieldPlaceHolders
{
    UIColor *color = [UIColor whiteColor];
    self.answer1.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Answer 1" attributes:@{NSForegroundColorAttributeName: color}];
    self.answer2.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Answer 2" attributes:@{NSForegroundColorAttributeName: color}];
    self.answer3.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Answer 3" attributes:@{NSForegroundColorAttributeName: color}];
    self.answer4.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Answer 4" attributes:@{NSForegroundColorAttributeName: color}];
    self.bibleReference.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Scripture Reference" attributes:@{NSForegroundColorAttributeName: color}];
    self.answer1.layer.sublayerTransform = CATransform3DMakeTranslation(12, 0, 0);
    self.answer2.layer.sublayerTransform = CATransform3DMakeTranslation(12, 0, 0);
    self.answer3.layer.sublayerTransform = CATransform3DMakeTranslation(12, 0, 0);
    self.answer4.layer.sublayerTransform = CATransform3DMakeTranslation(12, 0, 0);
    self.bibleReference.layer.sublayerTransform = CATransform3DMakeTranslation(50, 0, 0);
}

#pragma mark - UITextField Delegate Methods
-(void)setupTextFields
{
    self.questionText.delegate = self;
    self.answer1.delegate = self;
    self.answer2.delegate = self;
    self.answer3.delegate = self;
    self.answer4.delegate = self;
    self.bibleReference.delegate = self;
    self.pickerView = [[BTPickerView alloc]initWithFrame:CGRectMake(0, 260, 320, 200)];
    self.pickerView.senderObject = self.bibleReference;
    [self.bibleReference setInputView:self.pickerView];
    
}
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    textField.placeholder = nil;
    CGPoint pt;
    CGRect rc = [textField bounds];
    rc = [textField convertRect:rc toView:self.scrollView];
    pt = rc.origin;
    pt.x = 0;
    pt.y -= 60;
    [self.scrollView setContentOffset:pt animated:YES];
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField.tag == 6) {
        [self.scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.scrollView setContentOffset:svos animated:YES];
    NSInteger nextTag = textField.tag + 1;
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        [nextResponder becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }
    return YES;
}
-(void)textViewDidBeginEditing:(UITextView *)textView
{
    textView.text = nil;
}
-(void)prepareQuestion
{
    self.question = [[BTQuestion alloc]init];
    self.question.questionText = self.questionText.text;
    self.question.choice1 = self.answer1.text;
    self.question.choice2 = self.answer2.text;
    self.question.choice3 = self.answer3.text;
    self.question.choice4 = self.answer4.text;
    self.question.bookId = self.pickerView.question.bookId;
    self.question.chapter = self.pickerView.question.chapter;
    self.question.startVerse = self.pickerView.question.startVerse;
    self.question.endVerse = self.pickerView.question.endVerse;
    self.question.bibleReferenceText = self.bibleReference.text;
    for(int i=100;i<400;i=i+100){
        UISwitch *refSwitch = (UISwitch *)[self.view viewWithTag:i];
        if (refSwitch.on) {
            self.question.answer = [NSNumber numberWithInt:(i/100)];
        }
    }
}

- (IBAction)submitTouched:(id)sender {
    [self.activityView startAnimating];
    [self prepareQuestion];
    BTQuestionDataSource *datasource = [[BTQuestionDataSource alloc]init];
    datasource.delegate = self;
    [datasource addQuestion:self.question];
}
-(void)didAddedQuestion:(NSString *)data
{
    [self.activityView stopAnimating];
    self.success = YES;
    [self showMessage:@"Your question has been submitted. We will review and include into our question database." withTitle:@"Success"];
}
-(void)didFailedAddingQuestion:(NSString *)error
{
    [self.activityView stopAnimating];
    [self showMessage:@"Error adding your question. Please try again." withTitle:NSLocalizedString(@"ALERT",@"")];
}
-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (self.success)
    {
        [self performSegueWithIdentifier:@"mainSegue" sender:self];
    }
}
-(void)didReceiveQuestion:(NSString *)data
{}
-(void)didReceiveQuestionError:(NSString *)error
{}
// Show an alert message
- (void)showMessage:(NSString *)text withTitle:(NSString *)title
{
    [[[UIAlertView alloc] initWithTitle:title
                                message:text
                               delegate:self
                      cancelButtonTitle:NSLocalizedString(@"OK_BUTTON", @"")
                      otherButtonTitles:nil] show];
}
@end
