//
//  BTMessagesDataSource.m
//  TriviaGame
//
//  Created by Natalia Sibaja on 4/25/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import "BTMessagesDataSource.h"
#import "Messages+BTMessages.h"
#import "BTManagedObjectContext.h"

@implementation BTMessagesDataSource
-(void)saveInContext:(Messages *)message{
    [message save];
}
-(void)eraseFromContext:(Messages *)message{
    [message erase];
}

-(NSArray *)getMessagesFromContext{
    NSFetchRequest * request = [[NSFetchRequest alloc]init];
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"Messages"
                                                         inManagedObjectContext:[BTManagedObjectContext sharedInstance].managedObjectContext];
    [request setEntity:entityDescription];
    NSError *error=nil;
    NSArray * mutableFetchResults = [[BTManagedObjectContext sharedInstance].managedObjectContext executeFetchRequest:request error:&error];
    if (mutableFetchResults == nil) {
        if (DEBUG_COREDATA&&DEBUG_ENABLED) {
            //NSLog(@"CoreData:Error selecting messages");
        }
        return nil;
    }
    else{
        if ([mutableFetchResults count]>0) {
            return mutableFetchResults;
        }
        else{
            return nil;
        }
    }
}
-(Messages *)insertMessageInContext{
    Messages *message = nil;
    message = [NSEntityDescription insertNewObjectForEntityForName:@"Messages"
                                         inManagedObjectContext:[BTManagedObjectContext sharedInstance].managedObjectContext];
    return message;
}
@end
