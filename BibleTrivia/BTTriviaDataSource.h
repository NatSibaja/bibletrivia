//
//  BTTriviaDataSource.h
//  BibleTrivia
//
//  Created by Natalia Sibaja on 3/31/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Trivia;

@interface BTTriviaDataSource : NSObject

@property (nonatomic,strong) id delegate;

//Save the trivia in the data store
-(void)saveInContext:(Trivia *)trivia;

//Erase the trivia from the data store
-(void)eraseFromContext:(Trivia *)trivia;

//Gets the trivia from the data store. Returs nil if there isn't any user or if there was an error
-(Trivia *)getTriviaFromContext;

//Inserts the trivia in the context and returs the entity. NOTE: the user isn't saved, it just lives in the current context
-(Trivia *)insertTriviaInContext;
@end
