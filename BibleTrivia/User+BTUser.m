//
//  User+BTUser.m
//  BibleTrivia
//
//  Created by Natalia Sibaja on 2/25/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import "User+BTUser.h"
#import "BTManagedObjectContext.h"
@implementation User (BTUser)
-(void)save{
    NSError *error = nil;
    if (![[BTManagedObjectContext sharedInstance].managedObjectContext  save:&error]) {
        if (DEBUG_COREDATA) {
            NSLog(@"CoreData:Error saving the user.%@",error);
        }
    }
}

-(void)erase{
    NSError *error = nil;
    [[BTManagedObjectContext sharedInstance].managedObjectContext deleteObject:self];
    if (![[BTManagedObjectContext sharedInstance].managedObjectContext  save:&error]) {
        if (DEBUG_COREDATA) {
            NSLog(@"CoreData:Failed deleting the user.%@",error);
        }
    }
}
@end
