//
//  BTQuestionsViewController.m
//  BibleTrivia
//
//  Created by Natalia Sibaja on 2/21/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import "BTQuestionsViewController.h"
#import "BTQuestionDataSource.h"
#import "BTQuestionDelegate.h"
#import "BTQuestion.h"
#import "BTBannerView.h"
#import "Trivia.h"
#import "Trivia+BTTrivia.h"
#import "BTTriviaDataSource.h"
#import "BTEndGameViewController.h"
#import "BTBibleViewController.h"
#import "BTAnswerDataSource.h"
#import "BTAnswerDelegate.h"
#import "BTCustomActivityView.h"
#import "BTUserDataSource.h"
#import "User+BTUser.h"

@interface BTQuestionsViewController ()<UIAlertViewDelegate, BTAnswerDelegate>
@property (nonatomic) IBOutlet UIBarButtonItem* revealButtonItem;
@property (weak, nonatomic) IBOutlet UILabel *questionTextView;
@property (weak, nonatomic) IBOutlet UIButton *choice1;
@property (weak, nonatomic) IBOutlet UIButton *choice2;
@property (weak, nonatomic) IBOutlet UIButton *choice3;
@property (weak, nonatomic) IBOutlet UIButton *choice4;
@property (weak, nonatomic) IBOutlet UILabel *label1;
@property (weak, nonatomic) IBOutlet UILabel *label2;
@property (weak, nonatomic) IBOutlet UILabel *label3;
@property (weak, nonatomic) IBOutlet UILabel *label4;

@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (weak, nonatomic) IBOutlet BTCustomActivityView *activityView;
@property (weak, nonatomic) IBOutlet BTBannerView *bottomBanner;
@property (strong,nonatomic) BTQuestion *question;
@property (strong,nonatomic) Trivia *triviaGame;

@end

@implementation BTQuestionsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.activityView startAnimating];
    [self setupGame];
    [self setupView];
    [self setupSideBar];
    [self requestQuestion];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
-(void)viewDidAppear:(BOOL)animated
{
    if (self.triviaGame.totalQuestions.intValue == NO_QUESTIONS) {
        [self performSegueWithIdentifier:@"endGameSegue" sender:self];
    }
}
-(void)viewWillAppear:(BOOL)animated
{
    self.screenName = @"Game Screen";
    [Flurry logEvent:@"Game Screen"];
    
    if (self.triviaGame.score == nil) {
        [self.navigationItem setTitle:[NSString stringWithFormat:@"%i of %d Q's Correct",0,NO_QUESTIONS]];
    }
    else
    {
        [self.navigationItem setTitle:[NSString stringWithFormat:@"%@ of %d Q's Correct",self.triviaGame.score,NO_QUESTIONS]];
    }
}
-(void)setupView
{
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]];
    if (self.triviaGame.score == nil) {
        [self.navigationItem setTitle:[NSString stringWithFormat:@"%i of %d Q's Correct",0,NO_QUESTIONS]];
    }
    else
    {
     [self.navigationItem setTitle:[NSString stringWithFormat:@"%@ of %d Q's Correct",self.triviaGame.score,NO_QUESTIONS]];
    }
    
    int total = self.triviaGame.totalQuestions.intValue;
    [self.scoreLabel setText:[NSString stringWithFormat:@"%d",(total+1)]];
}
-(void)setupSideBar
{
    [self.revealButtonItem setTarget: self.revealViewController];
    [self.revealButtonItem setAction: @selector( revealToggle: )];
    [self.navigationController.navigationBar addGestureRecognizer: self.revealViewController.panGestureRecognizer];
}
-(void)setupGame
{
    self.question = [[BTQuestion alloc]init];
    BTTriviaDataSource *datasource = [[BTTriviaDataSource alloc]init];
    self.triviaGame = [datasource getTriviaFromContext];
    if (self.triviaGame == nil){
        self.triviaGame = [datasource insertTriviaInContext];
    }
    self.triviaGame.score = 0;
    self.triviaGame.totalQuestions = 0;
    [self.triviaGame save];
}
#pragma mark
#pragma Question Delegate
-(void)requestQuestion
{
    [self.activityView startAnimating];
    BTQuestionDataSource *datasource = [[BTQuestionDataSource alloc]init];
    datasource.delegate = self;
    [datasource requestQuestion];
    [self.bottomBanner changeAd];
}
-(void)didReceiveQuestion:(NSDictionary *)data
{
    [self performSelectorOnMainThread:@selector(setQuestionObject:) withObject:data waitUntilDone:YES];
}

-(void)setQuestionObject:(NSDictionary *)data
{
    NSDictionary *questionData = [data objectForKey:@"data"];
    self.question.questionId = [NSNumber numberWithInt: [questionData[@"questionId"] intValue]];
    self.question.questionText = questionData[@"question"];
    self.question.choice1 = questionData[@"choice1"];
    self.question.choice2 = questionData[@"choice2"];
    self.question.choice3 = questionData[@"choice3"];
    self.question.choice4 = questionData[@"choice4"];
    self.question.answer = [NSNumber numberWithInt: [questionData[@"answer"] intValue]];
    self.question.answerText = [questionData objectForKey:[NSString stringWithFormat:@"choice%i",[questionData[@"answer"] intValue]]];
    self.question.bibleReferenceText = questionData[@"bibleReferenceText"];
    self.question.bibleReferenceUrl = questionData[@"bibleReference"];
    [self setItemsInView];
}
-(void)setItemsInView
{
    int total = self.triviaGame.totalQuestions.intValue;
    [self.scoreLabel setText:[NSString stringWithFormat:@"%d",(total+1)]];

    [self.questionTextView setText:self.question.questionText];
    [self.label1 setText: self.question.choice1];
    [self.label2 setText: self.question.choice2];
    [self.label3 setText: self.question.choice3];
    [self.label4 setText: self.question.choice4];
    [self.activityView stopAnimating];
}

-(void)showResult:(NSString *)message andTitle:(NSString *)title andButton:(NSString *)button
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:@"See Reference" otherButtonTitles: button,nil];
    [alert show];
}
-(void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (self.triviaGame.totalQuestions.intValue == NO_QUESTIONS) {
        float totalQuestions = NO_QUESTIONS;
        float score = self.triviaGame.score.floatValue;
        score = (score/totalQuestions)*100;
        NSNumber *newScore = [NSNumber numberWithFloat:score];
        [self.triviaGame saveBestRound:newScore];
    }
}
- (IBAction)answerChosen:(id)sender {
   
    UIButton *button = (UIButton *)sender;
    
    self.triviaGame.totalQuestions = [NSNumber numberWithInt:[self.triviaGame.totalQuestions intValue] + 1];
    if ([self.question isCorrect:[NSNumber numberWithInt:button.tag]]) {
        [self showResult:[NSString stringWithFormat: @"%@\n Reference:\n%@", NSLocalizedString(@"CORRECT_MESSAGE", @""), self.question.bibleReferenceText] andTitle:NSLocalizedString(@"CORRECT", @"") andButton:@"OK"];
        self.triviaGame.score = [NSNumber numberWithInt:[self.triviaGame.score intValue] + 1];
    }
    else{
        NSString *message = [[NSString alloc]init];
        if (self.question.bibleReferenceText != nil){
            message = [NSString stringWithFormat:@"The correct answer is:\n%@\n Reference:\n%@",self.question.answerText, self.question.bibleReferenceText];
        }
        else{
            message = [NSString stringWithFormat:@"The correct answer is:\n%@\n",self.question.answerText];
        }
        [self showResult:message andTitle:NSLocalizedString(@"INCORRECT", @"") andButton:NSLocalizedString(@"NEXT_BUTTON", @"")];
    }
    if (self.triviaGame.score == nil) {
    [self.navigationItem setTitle:[NSString stringWithFormat:@"%i of %@ Q's Correct",0,self.triviaGame.totalQuestions]];
    }
    else{
          [self.navigationItem setTitle:[NSString stringWithFormat:@"%@ of %@ Q's Correct",self.triviaGame.score,self.triviaGame.totalQuestions]];
    }
    
   [self submitAnswer:self.question.questionId andAnswer:[NSNumber numberWithInt:button.tag]];
}
-(void)submitAnswer:(NSNumber *)questionId andAnswer:(NSNumber *)answer
{
    BTUserDataSource *datasource = [[BTUserDataSource alloc]init];
    datasource.delegate = self;
    User *user = [datasource getUserFromContext];
    if (user != nil){
        BTAnswerDataSource *datasource = [[BTAnswerDataSource alloc]init];
        datasource.delegate = self;
        [datasource postAnswer:questionId andAnswer:answer];
    }
}

-(void)didPostedAnswer
{
    //NSLog(@"posted answer");
}
-(void)didPostAnswerFail:(NSString *)errorMessage
{
    //NSLog(@"error posting answer");
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        [self requestQuestion];
        [self performSegueWithIdentifier:@"bibleSegue" sender:self];
    }
    else{
        if (self.triviaGame.totalQuestions.intValue == NO_QUESTIONS) {
            [self performSegueWithIdentifier:@"endGameSegue" sender:self];
        }
        else{
            [self requestQuestion];
        }
    }
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"bibleSegue"]) {
        BTBibleViewController *vc = [segue destinationViewController];
        vc.bibleURL = self.question.bibleReferenceUrl;
    }
    else{
        BTEndGameViewController *vc = [segue destinationViewController];
        vc.trivia = self.triviaGame;
    }
}
@end

