//
//  BTLaunchViewController.m
//  BibleTrivia
//
//  Created by Natalia Sibaja on 3/11/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import "BTLaunchViewController.h"
#import "UIButton+BTButton.h"
#import "BTAppDelegate.h"
#import "BTUserDataSource.h"
#import "BTLoginDataSource.h"
#import "BTLoginDelegate.h"
#import "User.h"
#import "BTBannerView.h"
#import "BTCustomActivityView.h"
#import "BTUserDataSource.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface BTLaunchViewController ()<BTLoginDelegate,UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UIButton *registerButton;
@property (weak, nonatomic) IBOutlet UIButton *laterButton;
@property (weak, nonatomic) IBOutlet BTCustomActivityView *activityView;
@property bool subscribe;
typedef void (^RPSBlock)(void);

@end

@implementation BTLaunchViewController
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupView];
}

- (IBAction)facebookButtonTouched:(id)sender{
    [self.activityView startAnimating];
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithReadPermissions:@[@"public_profile", @"email", @"user_friends"] handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if (error) {
            // Process error
        } else if (result.isCancelled) {
            // Handle cancellations
        } else {
            if ([result.grantedPermissions containsObject:@"email"]) {
                [self loginAllPass];
            }
        }
    }];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    if ([FBSDKAccessToken currentAccessToken] || [self userExists]){
        [self performSegueWithIdentifier:@"mainSegue" sender:self];
    }
    self.screenName = @"Launch Screen";
    [Flurry logEvent:@"Launch Screen"];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    
    NSDate *savedDate = [[NSUserDefaults standardUserDefaults] objectForKey:@"adTimeout"];
    NSDateComponents *dateComponents = [[NSCalendar currentCalendar] components:NSCalendarUnitDay fromDate:savedDate toDate:[NSDate date] options:0];
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"firstRun"]) {
    if ([dateComponents day] >= 1)
    {
        NSDate *today = [[NSDate alloc]init];
        [[NSUserDefaults standardUserDefaults] setObject:today forKey:@"adTimeout"];
        [self showPopUp];
    }
    }
    else
    {
        [[NSUserDefaults standardUserDefaults] setBool:FALSE forKey:@"firstRun"];
    }
    
}
- (void) viewWillDisappear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}
-(void)setupView
{
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]];
    [self.loginButton fillGradientBackground:DARK_GREEN :LIGHT_GREEN];
    [self.facebookButton setBackgroundColor:BLUE_BUTTON];
    [self.registerButton setBackgroundColor:BLUE_BUTTON];
    [self.laterButton setBackgroundColor:BLUE_BUTTON];
}
-(BOOL)prefersStatusBarHidden { return YES; }

- (void)showPopUp
{
    // Override point for customization after application launch.
    BTBannerView *splash = [[BTBannerView alloc]init];
    splash.bannerType = 1;
    splash.viewController = self;
    [self.view addSubview:splash];
    [splash showAd];
}

#pragma mark
#pragma Facebook Delegate Methods

- (void)loginAllPass
{
    [self.activityView stopAnimating];
    __block NSString *email;
    [self showAlert:NSLocalizedString(@"Welcome!", @"") message:NSLocalizedString(@"SUSCRIBE_MESSAGE", @"")];
    if ([FBSDKAccessToken currentAccessToken]) {
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:nil]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
             if (!error) {
                 email = [result objectForKey:@"email"];
                 BTLoginDataSource *datasource = [[BTLoginDataSource alloc]init];
                 datasource.delegate = self;
                 [datasource login:[FBSDKProfile currentProfile].name
                                  :[FBSDKProfile currentProfile].lastName
                                  :email
                                  :[FBSDKProfile currentProfile].userID
                                  :[FBSDKAccessToken currentAccessToken].tokenString
                  ];
             }
         }];
    }
}

-(void)loginPassed:(User *)user
{
    if (self.subscribe) {
        BTUserDataSource *datasource = [[BTUserDataSource alloc] init];
        datasource.delegate = self;
        [datasource subscribeNewsletter:user];
    }
    [self performSegueWithIdentifier:@"mainSegue" sender:self];
}
-(void)loginFailed:(NSString *)errorMessage
{
    [self showMessage:errorMessage withTitle:NSLocalizedString(@"ALERT",@"")];

}
// Show an alert message
- (void)showMessage:(NSString *)text withTitle:(NSString *)title
{
    [[[UIAlertView alloc] initWithTitle:title
                                message:text
                               delegate:self
                      cancelButtonTitle:NSLocalizedString(@"OK_BUTTON", @"")
                      otherButtonTitles:nil] show];
}
-(BOOL)userExists
{
    BTUserDataSource *datasource = [[BTUserDataSource alloc]init];
    User *user = [datasource getUserFromContext];
    if (user == nil) {
        return NO;
    }
    else{
        return YES;
    }
}
-(void)showAlert:(NSString *)title message:(NSString *)text
{
    UIAlertView *customAlert = [[UIAlertView alloc] initWithTitle:title message:text delegate:self cancelButtonTitle:NSLocalizedString(@"NO_THANKS", @"")  otherButtonTitles:NSLocalizedString(@"YES", @""),nil];
    [customAlert show];
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        self.subscribe = YES;
    }
}
-(void)didSubscribe:(NSDictionary *)userData
{
    NSLog(@"user subscribed");
}
-(void)didSubscribeError:(NSDictionary *)userData
{
    NSLog(@"Error on subscribe user");
    
}
@end
