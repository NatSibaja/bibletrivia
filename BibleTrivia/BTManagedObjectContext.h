//
//  BTManagedObjectContext.h
//  BibleTrivia
//
//  Created by Natalia Sibaja on 2/25/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BTManagedObjectContext : NSObject{
    
}
@property (nonatomic,strong) NSManagedObjectContext *managedObjectContext;
+(BTManagedObjectContext *) sharedInstance;

@end
