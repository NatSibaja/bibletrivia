//
//  BTQuestion.m
//  BibleTrivia
//
//  Created by Natalia Sibaja on 2/21/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import "BTQuestion.h"

@implementation BTQuestion

@synthesize questionText;
@synthesize choice1;
@synthesize choice2;
@synthesize choice3;
@synthesize choice4;
@synthesize answer;
@synthesize answerText;
@synthesize bibleReferenceText;
@synthesize bibleReferenceUrl;
@synthesize bookId;
@synthesize categoryId;
@synthesize chapter;
@synthesize startVerse;
@synthesize endVerse;

-(BOOL)isCorrect:(NSNumber *)choice
{
    if (choice == self.answer){
        return YES;
    }
    else{
        return NO;
    }
}
@end
