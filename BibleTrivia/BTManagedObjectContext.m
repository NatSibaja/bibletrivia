//
//  BTManagedObjectContext.m
//  BibleTrivia
//
//  Created by Natalia Sibaja on 2/25/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import "BTManagedObjectContext.h"

static BTManagedObjectContext * _sharedInstance;

@implementation BTManagedObjectContext

+(BTManagedObjectContext *) sharedInstance{
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        _sharedInstance = [[self alloc] init];
    });
    
    return _sharedInstance;
}

@end