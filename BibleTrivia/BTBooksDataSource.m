//
//  BTBooksDataSource.m
//  BibleTrivia
//
//  Created by Leonardo Ortiz on 3/24/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import "BTBooksDataSource.h"
#import "BTUserDataSource.h"
#import "User.h"
#import "BTUrlSession.h"
#import "NSData+Additions.h"
#import "Books.h"
#import "BTManagedObjectContext.h"
#import "BTApplicationDataSource.h"
#import "Application.h"
#import "Application+BTApplication.h"

@interface BTBooksDataSource()
-(Books *)insertBookInContext;
@end

@implementation BTBooksDataSource

#pragma mark -
#pragma mark Request book methods
-(void)requestBooks{
    Application *app = [[[BTApplicationDataSource alloc]init]getApplication];
    if ([app.books anyObject] == nil) {
        User *user = [[[BTUserDataSource alloc]init]getUserFromContext];
        BTUrlSession *btUrlSession = [[BTUrlSession alloc]init];
        btUrlSession.delegate = self;
        btUrlSession.didReceiveData = NSSelectorFromString(@"didReceiveBooks:");
        btUrlSession.didReceiveError = NSSelectorFromString(@"didFailedReceivingBooks:");
        NSString *authStr = [NSString stringWithFormat:@"%@:%@", API_KEY,user.authenticationKey];
        NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
        NSString *authValue = [NSString stringWithFormat:@"Basic %@", [authData base64Encoding]];
        NSString *stringUrl = [NSString stringWithFormat:@"%@%@", API_URL,BOOKS_URL];
        [btUrlSession doGetRequest:stringUrl withParams:nil andHeaders:@{@"Content-Type":@"application/json",@"Authorization":authValue}];
    }
    else{
        //If the books exist in the DB just return them
        SEL selector = NSSelectorFromString(@"didReceiveBooks:");
        if ([self.delegate respondsToSelector:selector]) {
            [self.delegate performSelectorOnMainThread:selector withObject:[self getAllBooks] waitUntilDone:YES];
        }
    }
    
}

-(void)didReceiveBooks:(NSData *)data{
    NSError *error;
    NSDictionary *dataDicc = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    if (error==nil) {
        NSString *message = [[dataDicc objectForKey:@"resultType"]objectForKey:@"message"];
        if ([message isEqualToString:@"Success"]) {
            NSArray *booksArray = [dataDicc objectForKey:@"data"];
            Application *app = [[[BTApplicationDataSource alloc]init]getApplication];
            for(NSDictionary *bookInfo in booksArray){
                Books *book = [self insertBookInContext];
                book.bookId = [NSNumber numberWithInt:[[bookInfo objectForKey:@"bookId"]intValue]];
                book.bookName = [bookInfo objectForKey:@"bookName"];
                book.chapterCount = [NSNumber numberWithInt:[[bookInfo objectForKey:@"chapterCount"]intValue]];
                book.application = app;
            }
            [app save];
            SEL selector = NSSelectorFromString(@"didReceiveBooks:");
            if ([self.delegate respondsToSelector:selector]) {
                [self.delegate performSelectorOnMainThread:selector withObject:[self getAllBooks] waitUntilDone:YES];
            }
        }
        else{
            [self didFailedReceivingBooks:[dataDicc objectForKey:@"message"]];
        }

    }
}

-(void)didFailedReceivingBooks:(NSString *)errorMessage{
    SEL selector = NSSelectorFromString(@"didFailedReceivingBooks:");
    if ([self.delegate respondsToSelector:selector]) {
        [self.delegate performSelectorOnMainThread:selector withObject:errorMessage waitUntilDone:YES];
    }
}

#pragma mark -
#pragma mark Core data methods
-(Books *)insertBookInContext{
    Books *book = nil;
    book = [NSEntityDescription insertNewObjectForEntityForName:@"Books"
                                         inManagedObjectContext:[BTManagedObjectContext sharedInstance].managedObjectContext];
    return book;
}

-(NSArray *)getAllBooks{
    Application *app = [[[BTApplicationDataSource alloc]init]getApplication];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"bookId" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    return [[app.books allObjects]sortedArrayUsingDescriptors:sortDescriptors];
}

-(Books *)getBookById:(NSNumber *)bookId{
    Application *app = [[[BTApplicationDataSource alloc]init]getApplication];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"bookId == %@", bookId];
    NSArray *filteredArray = [[app.books filteredSetUsingPredicate:predicate]allObjects];
    id firstFoundObject = nil;
    if ([filteredArray count] > 0) {
        firstFoundObject = [filteredArray objectAtIndex:0];
    }
    return firstFoundObject;
}


@end
