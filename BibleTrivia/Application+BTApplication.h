//
//  Application+BTApplication.h
//  BibleTrivia
//
//  Created by Leonardo Ortiz on 3/24/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import "Application.h"

@interface Application (BTApplication)
-(void)save;
-(void)erase;
@end
