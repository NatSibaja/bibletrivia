//
//  User+BTUser.h
//  BibleTrivia
//
//  Created by Natalia Sibaja on 2/25/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import "User.h"

@interface User (BTUser)
-(void)save;
-(void)erase;
@end
