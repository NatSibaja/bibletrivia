//
//  BTEndGameViewController.h
//  BibleTrivia
//
//  Created by Natalia Sibaja on 3/3/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Trivia.h"

typedef enum {
    Church_Goer=20,
    Seminary_Graduate = 40,
    Bible_College_Student = 60,
    Theologian = 80,
    Top_Biblical_Scholar = 100
} Level;

@interface BTEndGameViewController : GAITrackedViewController

@property (strong, nonatomic) IBOutlet UILabel *gameDoneLabel;
@property (strong, nonatomic) Trivia *trivia;
@end
