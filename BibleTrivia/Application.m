//
//  Application.m
//  BibleTrivia
//
//  Created by Leonardo Ortiz on 4/9/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import "Application.h"
#import "Ad.h"
#import "Books.h"
#import "User.h"


@implementation Application

@dynamic books;
@dynamic user;
@dynamic ads;

@end
