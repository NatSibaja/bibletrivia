//
//  Trivia.m
//  BibleTrivia
//
//  Created by Leonardo Ortiz on 4/9/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import "Trivia.h"


@implementation Trivia

@dynamic bestRound;
@dynamic score;
@dynamic totalQuestions;

@end
