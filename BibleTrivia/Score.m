//
//  Score.m
//  BibleTrivia
//
//  Created by Leonardo Ortiz on 4/9/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import "Score.h"
#import "User.h"


@implementation Score

@dynamic correct;
@dynamic percentCorrect;
@dynamic questions;
@dynamic userId;
@dynamic userName;
@dynamic userInfo;

@end
