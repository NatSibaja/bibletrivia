//
//  NSData+Additions.h
//  BibleTrivia
//
//  Created by Natalia Sibaja on 2/25/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (MBBase64)

+ (id)dataWithBase64EncodedString:(NSString *)string;     //  Padding '=' characters are optional. Whitespace is ignored.
- (NSString *)base64Encoding;

@end
