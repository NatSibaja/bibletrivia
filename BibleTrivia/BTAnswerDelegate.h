//
//  BTAnswerDelegate.h
//  BibleTrivia
//
//  Created by Leonardo Ortiz on 3/24/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol BTAnswerDelegate <NSObject>

//This method is called after the answer was successfully accepted by the server
-(void)didPostedAnswer;
//This method is called if the post fails
-(void)didPostAnswerFail:(NSString *)errorMessage;

@end
