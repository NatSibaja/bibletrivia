//
//  BTMessageCell.m
//  TriviaGame
//
//  Created by Natalia Sibaja on 4/28/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import "BTMessageCell.h"

@implementation BTMessageCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
