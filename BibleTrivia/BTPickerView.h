//
//  BTPickerView.h
//  TriviaGame
//
//  Created by Natalia Sibaja on 5/16/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BTQuestion.h"

@interface BTPickerView : UIView
@property (strong, nonatomic)IBOutlet id senderObject;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (strong, nonatomic)IBOutlet UIBarButtonItem *cancelButton;
@property (strong, nonatomic)IBOutlet UIBarButtonItem *doneButton;
@property (strong, nonatomic)IBOutlet UIPickerView *pickerView;
@property (strong, nonatomic) BTQuestion *question;

-(IBAction)cancelTouched:(id)sender;
-(IBAction)doneTouched:(id)sender;

@end
