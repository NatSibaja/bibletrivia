//
//  BTLoginDelegate.h
//  BibleTrivia
//
//  Created by Natalia Sibaja on 2/26/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import <Foundation/Foundation.h>
@class User;

@protocol BTLoginDelegate <NSObject>
-(void)loginFailed:(NSString *)errorMessage;
-(void)loginPassed:(User *)user;
@end