//
//  BTUserDataSource.h
//  BibleTrivia
//
//  Created by Natalia Sibaja on 2/25/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import <Foundation/Foundation.h>
@class User;

@interface BTUserDataSource : NSObject

@property (nonatomic,strong) id delegate;

//Save the user in the data store
-(void)saveInContext:(User *)user;

//Erase the user from the data store
-(void)eraseFromContext:(User *)user;

//Gets the user from the data store. Returs nil if there isn't any user or if there was an error
-(User *)getUserFromContext;

//Inserts the user in the context and returs the entity. NOTE: the user isn't saved, it just lives in the current context
-(User *)insertUserInContext;

//Requests the user info from the server
-(void)getUserFromServer:(NSString *)authKey;

//Updates the user's info into the server
-(void)updateUserIntoServer:(NSMutableDictionary *)params andUser:(User *)user;

//Add user into the server
-(void)addUserIntoServer:(User *)user;

//Retrieve the user stats
-(void)getUserStats:(User *)user;

//Subscribe user to newsletter
-(void)subscribeNewsletter:(User *)user;

@end
