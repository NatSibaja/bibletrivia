//
//  Ad.h
//  BibleTrivia
//
//  Created by Leonardo Ortiz on 4/9/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Application;

@interface Ad : NSManagedObject

@property (nonatomic, retain) NSDate * expiration;
@property (nonatomic, retain) NSString * splashAd;
@property (nonatomic, retain) NSString * bottomAd;
@property (nonatomic, retain) Application *application;

@end
