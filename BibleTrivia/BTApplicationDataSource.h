//
//  BTApplicationDataSource.h
//  BibleTrivia
//
//  Created by Leonardo Ortiz on 3/24/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Application;

@interface BTApplicationDataSource : NSObject

-(Application *)getApplication;

@end
