//
//  BTMessagesDataSource.h
//  TriviaGame
//
//  Created by Natalia Sibaja on 4/25/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Messages;

@interface BTMessagesDataSource : NSObject
//Save the user in the data store
-(void)saveInContext:(Messages *)message;

//Erase the user from the data store
-(void)eraseFromContext:(Messages *)message;

//Gets the user from the data store. Returs nil if there isn't any user or if there was an error
-(NSArray *)getMessagesFromContext;

-(Messages *)insertMessageInContext;

@end
