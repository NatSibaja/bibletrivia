//
//  main.m
//  BibleTrivia
//
//  Created by Natalia Sibaja on 2/19/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BTAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([BTAppDelegate class]));
    }
}
