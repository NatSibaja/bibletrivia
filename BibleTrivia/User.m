//
//  User.m
//  TriviaGame
//
//  Created by Natalia Sibaja on 6/4/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import "User.h"
#import "Application.h"
#import "Score.h"


@implementation User

@dynamic accessToken;
@dynamic authenticationKey;
@dynamic email;
@dynamic facebookId;
@dynamic firstName;
@dynamic lastName;
@dynamic password;
@dynamic postalCode;
@dynamic userId;
@dynamic userScore;
@dynamic subscribe;
@dynamic application;
@dynamic userScores;

@end
