//
//  BTBibleViewController.m
//  BibleTrivia
//
//  Created by Natalia Sibaja on 3/25/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import "BTBibleViewController.h"
#import "BTCustomActivityView.h"

@interface BTBibleViewController ()<UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet BTCustomActivityView *activityView;
@end

@implementation BTBibleViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBar.topItem.title = @"Keep Playing";
    [self.activityView startAnimating];
    self.webView.delegate = self;
    NSURL *websiteUrl = [NSURL URLWithString:self.bibleURL];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:websiteUrl];
    [self.webView loadRequest:urlRequest];
}
-(void)viewWillAppear:(BOOL)animated
{
    self.screenName = @"Bible Reference";
    [Flurry logEvent:@"Bible Reference"];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma WebView Delegate
-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self.activityView stopAnimating];
}
@end
