//
//  BTAccountViewController.m
//  BibleTrivia
//
//  Created by Natalia Sibaja on 3/17/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import "BTAccountViewController.h"
#import "UIButton+BTButton.h"
#import <QuartzCore/QuartzCore.h>
#import "User.h"
#import "User+BTUser.h"
#import "BTUserDataSource.h"
#import "BTLoginViewController.h"

#import <FBSDKCoreKit/FBSDKCoreKit.h>

@interface BTAccountViewController ()<UITextFieldDelegate, UIAlertViewDelegate>

@property (nonatomic) IBOutlet UIBarButtonItem *revealButtonItem;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *confirmTextField;
@property (weak, nonatomic) IBOutlet UITextField *firstNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *zipTextField;
@property (weak, nonatomic) IBOutlet UIView *fadeView;
@property (weak, nonatomic) IBOutlet UIButton *logoutButton;
@property (weak, nonatomic) IBOutlet UIButton *updateButton;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;


@property (strong, nonatomic) User *user;

-(IBAction)logoutTouched:(id)sender;
-(IBAction)updateTouched:(id)sender;

@end

@implementation BTAccountViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupView];
    [self setupSideBar];
    [self requestUser];
}
-(void)viewWillAppear:(BOOL)animated
{
    self.screenName = @"My Account";
    [Flurry logEvent:@"My Account"];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setupView
{
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]]];
    [self.fadeView setBackgroundColor:BLUE_FADE];
    [self.updateButton fillGradientBackground:DARK_GREEN :LIGHT_GREEN];
    [self.emailTextField setBackgroundColor:BLUE_BUTTON];
    [self.emailTextField setBackgroundColor:BLUE_BUTTON];
    [self.passwordTextField setBackgroundColor:BLUE_BUTTON];
    [self.confirmTextField setBackgroundColor:BLUE_BUTTON];
    [self.firstNameTextField setBackgroundColor:BLUE_BUTTON];
    [self.lastNameTextField setBackgroundColor:BLUE_BUTTON];
    [self.zipTextField setBackgroundColor:BLUE_BUTTON];
    self.confirmTextField.delegate = self;
    self.passwordTextField.delegate = self;
    self.firstNameTextField.delegate = self;
    self.lastNameTextField.delegate = self;
    self.zipTextField.delegate = self;
    self.passwordTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Change Password" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    self.confirmTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Confirm Password" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    self.emailTextField.layer.sublayerTransform = CATransform3DMakeTranslation(50, 0, 0);
    self.passwordTextField.layer.sublayerTransform = CATransform3DMakeTranslation(50, 0, 0);
    self.confirmTextField.layer.sublayerTransform = CATransform3DMakeTranslation(50, 0, 0);
    self.firstNameTextField.layer.sublayerTransform = CATransform3DMakeTranslation(50, 0, 0);
    self.lastNameTextField.layer.sublayerTransform = CATransform3DMakeTranslation(50, 0, 0);
    self.zipTextField.layer.sublayerTransform = CATransform3DMakeTranslation(50, 0, 0);
}

-(void)setupSideBar
{
    [self.revealButtonItem setTarget: self.revealViewController];
    [self.revealButtonItem setAction: @selector( revealToggle: )];
    [self.navigationController.navigationBar addGestureRecognizer: self.revealViewController.panGestureRecognizer];
}
-(void)requestUser
{
    BTUserDataSource *datasource = [[BTUserDataSource alloc]init];
    datasource.delegate = self;
    self.user = [datasource getUserFromContext];
    if (self.user == nil){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"ALERT",@"") message:@"Please register or log in to access account settings." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        alert.delegate = self;
        [alert show];
        [self performSegueWithIdentifier:@"loginSegue" sender:self];
    }
    else{
        [self displayUser];
    }
    
}
-(void)displayUser
{
    [self.emailTextField setText:self.user.email];
    [self.firstNameTextField setText:self.user.firstName];
    [self.lastNameTextField setText:self.user.lastName];
    [self.zipTextField setText:self.user.postalCode];
}

-(void)updateUser:(NSMutableDictionary *)data
{
    [self.user save];
    BTUserDataSource *datasource = [[BTUserDataSource alloc]init];
    datasource.delegate = self;
    [datasource updateUserIntoServer:data andUser:self.user];
}

-(BOOL)textFieldShouldReturn:(UITextField*)textField;
{
    NSInteger nextTag = textField.tag + 1;
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        [nextResponder becomeFirstResponder];
    } else {
        CGPoint pt;
        pt.x = 0;
        pt.y = 0;
        [self.scrollView setContentOffset:pt animated:YES];
        [textField resignFirstResponder];
    }
    return NO; // We do not want UITextField to insert line-breaks.
}
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    CGPoint pt;
    CGRect rc = [textField bounds];
    rc = [textField convertRect:rc toView:self.scrollView];
    pt = rc.origin;
    pt.x = 0;
    pt.y -= 60;
    [self.scrollView setContentOffset:pt animated:YES];
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if(textField.tag == 2){
    if (![textField.text isEqualToString:self.passwordTextField.text]) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Both passwords must coincide." delegate:self cancelButtonTitle:NSLocalizedString(@"OK_BUTTON", @"") otherButtonTitles:nil];
        [alert show];
    }
    }
}
-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    [self.confirmTextField becomeFirstResponder];
}
-(IBAction)logoutTouched:(id)sender
{
    if ([FBSDKAccessToken currentAccessToken]) {
        FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
        [login logOut];
    }
    [self.user erase];
}
-(IBAction)updateTouched:(id)sender
{
    self.user.firstName = self.firstNameTextField.text;
    self.user.lastName = self.lastNameTextField.text;
    self.user.postalCode = self.zipTextField.text;
    self.user.password = self.passwordTextField.text;

}
- (void)showMessage:(NSString *)text withTitle:(NSString *)title
{
    [[[UIAlertView alloc] initWithTitle:title
                                message:text
                               delegate:self
                      cancelButtonTitle:NSLocalizedString(@"OK_BUTTON", @"")
                      otherButtonTitles:nil] show];
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    BTLoginViewController *vc = [segue destinationViewController];
    vc.navigationItem.hidesBackButton = YES;
}
@end
