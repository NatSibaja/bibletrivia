//
//  BTBannerView.h
//  BibleTrivia
//
//  Created by Natalia Sibaja on 4/7/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BTBannerView : UIView

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (strong, nonatomic) IBOutlet UIViewController *viewController;
@property DFPBannerView *bannerView_;
@property GADInterstitial *interstitial_;


@property int bannerType;

-(void)changeAd;
-(void)showAd;

@end
