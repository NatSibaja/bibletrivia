//
//  BTMessageCenterViewController.h
//  TriviaGame
//
//  Created by Natalia Sibaja on 5/20/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UAirship.h"
#import "UAInbox.h"
#import "UAInboxMessageListController.h"
#import "UAInboxMessageViewController.h"
#import "UAUtils.h"

@interface BTMessageCenterViewController : UIViewController

@end
