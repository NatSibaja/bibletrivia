//
//  Trivia+BTTrivia.m
//  BibleTrivia
//
//  Created by Natalia Sibaja on 3/31/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import "Trivia+BTTrivia.h"
#import "BTManagedObjectContext.h"

@implementation Trivia (BTTrivia)
-(void)save{
    NSError *error = nil;
    if (![[BTManagedObjectContext sharedInstance].managedObjectContext  save:&error]) {
        if (DEBUG_COREDATA) {
            NSLog(@"CoreData:Error saving the Game.%@",error);
        }
    }
}

-(void)erase{
    NSError *error = nil;
    [[BTManagedObjectContext sharedInstance].managedObjectContext deleteObject:self];
    if (![[BTManagedObjectContext sharedInstance].managedObjectContext  save:&error]) {
        if (DEBUG_COREDATA) {
            NSLog(@"CoreData:Failed deleting the Game.%@",error);
        }
    }
}
-(void)saveBestRound:(NSNumber *)newScore
{
    if (newScore > self.bestRound || self.bestRound == nil)
    {
        self.bestRound = newScore;
    }
    NSError *error = nil;
    if (![[BTManagedObjectContext sharedInstance].managedObjectContext  save:&error]) {
        if (DEBUG_COREDATA) {
            NSLog(@"CoreData:Error saving the Game.%@",error);
        }
    }

}
@end
