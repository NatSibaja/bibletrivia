//
//  BTAnswerDataSource.h
//  BibleTrivia
//
//  Created by Leonardo Ortiz on 3/24/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BTAnswerDataSource : NSObject
@property (strong,nonatomic) id delegate;
//Post the answer under the current user
-(void)postAnswer:(NSNumber *)questionId andAnswer:(NSNumber *)answer;
@end
