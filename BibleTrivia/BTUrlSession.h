//
//  IBUrlSession.h
//  Bible Trivia
//
//  Created by Leonardo Ortiz on 11/14/13.
//  Copyright (c) 2013 Salem. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BTUrlSession : NSObject

typedef enum{
    UNREACHABLE_NETWORK,
    NO_NETWORK_ERROR,
    JSON_PARSING_ERROR
}NSUrlErrorType;

typedef enum{
    KeyValue,
    JsonObject
}ParamFormat;


@property (nonatomic,strong) id delegate;
@property (nonatomic,assign) SEL didReceiveData;
@property (nonatomic,assign) SEL didReceiveError;



//Since objective-c doesn't support variable argumets we will code one method for every case

-(void) doPostRequest:(NSString *) stringUrl
           withParams:(NSMutableDictionary *)params;

-(void) doPostRequest:(NSString *)stringUrl
           withParams:(NSMutableDictionary *)params
           andHeaders:(NSDictionary *)headers;
-(void)doPostRequest:(NSString *)stringUrl
          withParams:(NSMutableDictionary *)params
          andHeaders:(NSDictionary *)headers
    withParamsFormat:(ParamFormat)paramsFormat;

-(void)doGetRequest:(NSString *)stringUrl
         withParams:(NSMutableDictionary *)params
         andHeaders:(NSDictionary *)headers;

-(void)doGetRequest:(NSString *)stringUrl
         withParams:(NSMutableDictionary *)params;

-(void)doDeleteRequest:(NSString *)stringUrl
            withParams:(NSMutableDictionary *)params;
-(void)doDeleteRequest:(NSString *)stringUrl
            withParams:(NSMutableDictionary *)params
            andHeaders:(NSDictionary *)headers;

-(void)doPutRequest:(NSString *)stringUrl
            withParams:(NSMutableDictionary *)params;
-(void)doPutRequest:(NSString *)stringUrl
            withParams:(NSMutableDictionary *)params
            andHeaders:(NSDictionary *)headers;



@end
