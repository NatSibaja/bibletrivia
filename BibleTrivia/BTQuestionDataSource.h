//
//  BTQuestionDataSource.h
//  BibleTrivia
//
//  Created by Natalia Sibaja on 2/21/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BTQuestion.h"
@interface BTQuestionDataSource : NSObject
@property (strong, nonatomic) id delegate;

-(void)requestQuestion;
-(void)addQuestion:(BTQuestion *)question;

@end
