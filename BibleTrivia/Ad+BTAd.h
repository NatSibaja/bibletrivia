//
//  Ad+BTAd.h
//  BibleTrivia
//
//  Created by Natalia Sibaja on 4/7/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import "Ad.h"

@interface Ad (BTAd)

-(BOOL)isValid;
-(void)save;
-(void)erase;

@end
