//
//  NSDate+BTDate.h
//  BibleTrivia
//
//  Created by Natalia Sibaja on 4/7/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (BTDate)
+ (NSDate *)dateByAddingHours:(NSInteger) hours toDate:(NSDate *)date;
+ (int)daysFromDate:(NSDate *)pDate;
@end
