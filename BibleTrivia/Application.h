//
//  Application.h
//  BibleTrivia
//
//  Created by Leonardo Ortiz on 4/9/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Ad, Books, User;

@interface Application : NSManagedObject

@property (nonatomic, retain) NSSet *books;
@property (nonatomic, retain) User *user;
@property (nonatomic, retain) Ad *ads;
@end

@interface Application (CoreDataGeneratedAccessors)

- (void)addBooksObject:(Books *)value;
- (void)removeBooksObject:(Books *)value;
- (void)addBooks:(NSSet *)values;
- (void)removeBooks:(NSSet *)values;

@end
