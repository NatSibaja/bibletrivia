//
//  Ad+BTAd.m
//  BibleTrivia
//
//  Created by Natalia Sibaja on 4/7/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import "Ad+BTAd.h"
#import "BTManagedObjectContext.h"

@implementation Ad (BTAd)

-(BOOL)isValid{
    NSDate *currentTime = [NSDate date];
    if ([currentTime compare:self.expiration]==NSOrderedDescending) {
        //current time is bigger than the expiration date. The ad is invalid
        [self erase];
        return NO;
    }
    else{
        return YES;
    }
}

-(void)erase{
    BTManagedObjectContext *context = [BTManagedObjectContext sharedInstance];
    NSError *error = nil;
    [context.managedObjectContext deleteObject:self];
    if (![context.managedObjectContext save:&error]) {
        if (DEBUG_ENABLED && DEBUG_COREDATA) {
            NSLog(@"Failed deleting the app entity");
        }
    }
    
}

-(void)save{
    BTManagedObjectContext *context = [BTManagedObjectContext sharedInstance];
    NSError *error = nil;
    [context.managedObjectContext save:&error];
    if (![context.managedObjectContext save:&error]) {
        if (DEBUG_ENABLED && DEBUG_COREDATA) {
            NSLog(@"Failed saving the app entity");
        }
    }
}
@end
