//
//  BTLoginViewController.h
//  BibleTrivia
//
//  Created by Natalia Sibaja on 2/26/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface BTLoginViewController : GAITrackedViewController

@end
