//
//  BTBibleViewController.h
//  BibleTrivia
//
//  Created by Natalia Sibaja on 3/25/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BTBibleViewController : GAITrackedViewController

@property (weak, nonatomic) NSString *bibleURL;
@end
