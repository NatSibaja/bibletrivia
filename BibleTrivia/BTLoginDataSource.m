//
//  BTLoginDataSource.m
//  BibleTrivia
//
//  Created by Natalia Sibaja on 2/25/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import "BTLoginDataSource.h"
#import "BTUserDataSource.h"
#import "BTUrlSession.h"
#import "User.h"
#import "User+BTUser.h"
#import "NSData+Additions.h"

#pragma clang diagnostic ignored "-Warc-performSelector-leaks"

@interface BTLoginDataSource()
-(void)failedLogin:(NSString *)errorMessage;
-(void)loginPassed:(User *)user;
-(void)sendLoginRequest:(NSDictionary *)params;
@end
@implementation BTLoginDataSource

#pragma mark -
#pragma mark - Login functions

-(void)login:(NSString *)email andPassword:(NSString *)password{
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjects:@[email,password] forKeys:@[@"email",@"password"]];
    [self sendLoginRequest:params];
}

-(void)login:(NSString *)firstName :(NSString *)lastName :(NSString *)email :(NSString *)facebookId :(NSString *)accessToken{
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjects:@[firstName,lastName,email,facebookId,accessToken] forKeys:@[@"firstName",@"lastName",@"email",@"facebookId",@"accessToken"]];
    [self sendLoginRequest:params];
    
}


-(void)sendLoginRequest:(NSMutableDictionary *)params{
    BTUrlSession *ibUrlSession = [[BTUrlSession alloc]init];
    ibUrlSession.delegate=self;
    ibUrlSession.didReceiveData = NSSelectorFromString(@"didReceiveLogin:");
    ibUrlSession.didReceiveError = NSSelectorFromString(@"didReceiveLoginError:");
    NSString *stringUrl = [NSString stringWithFormat:@"%@%@",API_URL, LOGIN_URL];
    NSString *authStr = [NSString stringWithFormat:@"%@:", API_KEY];
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", [authData base64Encoding]];
    [ibUrlSession doPostRequest:stringUrl withParams:params andHeaders:@{@"Content-Type":@"application/json",@"Authorization":authValue} withParamsFormat:JsonObject];
}


-(void)didReceiveLogin:(NSData *)data{
    NSError *error;
    NSDictionary *dataDicc = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    if (dataDicc!=nil) {
        NSDictionary *item = [dataDicc objectForKey:@"data"];
        NSArray *message = [dataDicc objectForKey:@"messages"];
        if (message==nil) {
            BTUserDataSource *userDataSource = [[BTUserDataSource alloc]init];
            User *user = [userDataSource getUserFromContext];
            if (user==nil) {
                user = [userDataSource insertUserInContext];
            }
            user.authenticationKey = [item objectForKey:@"userKey"];
            user.email = [item objectForKey:@"email"];
            user.firstName = [item objectForKey:@"firstName"];
            user.lastName = [item objectForKey:@"lastName"];
            user.userId = [item objectForKey:@"userId"];
            user.postalCode = [item objectForKey:@"postalCode"];
            [user save];
            [self loginPassed:user];
        }
        else{
            [self failedLogin:[message objectAtIndex:0]];
        }
    }
    else{
        [self failedLogin:@"Error parsing the login info"];
    }
}

-(void)didReceiveLoginError:(NSUrlErrorType)error{
    [self failedLogin:@"Networking error during login"];
}

-(void)failedLogin:(NSString *)errorMessage{
    SEL selector = NSSelectorFromString(@"loginFailed:");
    if ([self.delegate respondsToSelector:selector]) {
        [self.delegate performSelectorOnMainThread:selector withObject:errorMessage waitUntilDone:YES];
    }
}

-(void)loginPassed:(User *)user{
    SEL selector = NSSelectorFromString(@"loginPassed:");
    if ([self.delegate respondsToSelector:selector]) {
        [self.delegate performSelectorOnMainThread:selector withObject:user waitUntilDone:YES];
    }
}


@end
