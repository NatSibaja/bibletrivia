//
//  BTAnswerDataSource.m
//  BibleTrivia
//
//  Created by Leonardo Ortiz on 3/24/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import "BTAnswerDataSource.h"
#import "User.h"
#import "BTUserDataSource.h"
#import "BTUrlSession.h"
#import "NSData+Additions.h"

@implementation BTAnswerDataSource


-(void)postAnswer:(NSNumber *)questionId andAnswer:(NSNumber *)answer{
    BTUrlSession *btUrlSession = [[BTUrlSession alloc]init];
    btUrlSession.delegate = self;
    btUrlSession.didReceiveData = NSSelectorFromString(@"didPostedAnswer:");
    btUrlSession.didReceiveError = NSSelectorFromString(@"didPostAnswerFail:");
    BTUserDataSource *userDataSource = [[BTUserDataSource alloc]init];
    User *user = [userDataSource getUserFromContext];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:questionId forKey:@"questionId"];
    [params setObject:answer forKey:@"answer"];
    
    NSString *authStr = [NSString stringWithFormat:@"%@:%@", API_KEY,user.authenticationKey];
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", [authData base64Encoding]];
    NSString *stringUrl = [NSString stringWithFormat:@"%@%@", API_URL,ANSWERS_URL];
    [btUrlSession doPostRequest:stringUrl withParams:params andHeaders:@{@"Content-Type":@"application/json",@"Authorization":authValue} withParamsFormat:JsonObject];
}

-(void)didPostedAnswer:(NSData *)data{
    NSError *error;
    NSDictionary *dataDicc = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    if (error==nil) {
        NSString *message = [[dataDicc objectForKey:@"resultType"]objectForKey:@"message"];
        if ([message isEqualToString:@"Success"]) {
            SEL selector = NSSelectorFromString(@"didPostedAnswer");
            if ([self.delegate respondsToSelector:selector]) {
                [self.delegate performSelectorOnMainThread:selector withObject:dataDicc waitUntilDone:YES];
            }
        }
        else{
            [self didPostAnswerFail:[dataDicc objectForKey:@"message"]];
        }
    }
    else{
        [self didPostAnswerFail:@"Error parsing the JSON"];
    }
}

-(void)didPostAnswerFail:(NSString *)errorMessage{
    SEL selector = NSSelectorFromString(@"didPostAnswerFail:");
    if ([self.delegate respondsToSelector:selector]) {
        [self.delegate performSelectorOnMainThread:selector withObject:errorMessage waitUntilDone:YES];
    }
}

@end
