//
//  BTLoginViewController.m
//  BibleTrivia
//
//  Created by Natalia Sibaja on 2/26/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import "BTLoginViewController.h"
#import "BTLoginDelegate.h"
#import "BTLoginDataSource.h"
#import "BTUserDataSource.h"
#import "User.h"
#import "UIButton+BTButton.h"
#import "BTMainViewController.h"
#import "BTCustomActivityView.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "BTSettings.h"

@interface BTLoginViewController ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *emailText;
@property (weak, nonatomic) IBOutlet UITextField *passwordText;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UIButton *maybeLaterButton;
@property (weak, nonatomic) IBOutlet UIButton *facebookButton;
@property (weak, nonatomic) IBOutlet UIButton *registerButton;
@property (weak, nonatomic) IBOutlet BTCustomActivityView *activityView;

typedef void (^RPSBlock)(void);

- (IBAction)forgotPasswordClicked:(id)sender;
@end

@implementation BTLoginViewController
{
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    self.screenName = @"Login Screen";
    [Flurry logEvent:@"Login Screen"];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupView];
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setupView
{
    self.navigationController.navigationBarHidden = NO;
    UIColor *color = [UIColor whiteColor];
    self.emailText.delegate = self;
    self.emailText.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email Address" attributes:@{NSForegroundColorAttributeName: color}];
     self.passwordText.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{NSForegroundColorAttributeName: color}];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]];
    
    [self.loginButton fillGradientBackground:DARK_GREEN :LIGHT_GREEN];
    [self.maybeLaterButton setBackgroundColor:BLUE_BUTTON];

    [self.facebookButton setBackgroundColor:BLUE_BUTTON];
    [self.registerButton setBackgroundColor:BLUE_BUTTON];
    self.passwordText.layer.sublayerTransform = CATransform3DMakeTranslation(45, 0, 0);
    self.emailText.delegate = self;
    self.passwordText.delegate = self;
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    self.emailText.rightView = paddingView;
    self.emailText.rightViewMode = UITextFieldViewModeAlways;
    paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 45, 20)];
    self.emailText.leftView = paddingView;
    self.emailText.leftViewMode = UITextFieldViewModeAlways;
}
-(NSRange)visibleRangeOfTextView:(UITextView *)textView {
    CGRect bounds = textView.bounds;
    UITextPosition *start = [textView characterRangeAtPoint:bounds.origin].start;
    UITextPosition *end = [textView characterRangeAtPoint:CGPointMake(CGRectGetMaxX(bounds), CGRectGetMaxY(bounds))].end;
    return NSMakeRange([textView offsetFromPosition:textView.beginningOfDocument toPosition:start],
                       [textView offsetFromPosition:start toPosition:end]);
}
- (IBAction)loginClicked:(id)sender {
    [self.activityView startAnimating];
    [self.passwordText resignFirstResponder];
    BTLoginDataSource *datasource = [[BTLoginDataSource alloc]init];
    datasource.delegate = self;
    NSString *email = self.emailText.text;
    NSString *password = self.passwordText.text;
    [datasource login:email andPassword:password];
}
- (IBAction)forgotPasswordClicked:(id)sender {

}
#pragma mark -
#pragma Login Delegate Methods
-(void)loginPassed:(User *)user{
    [self performSegueWithIdentifier:@"mainSegue" sender:self];
}

-(void)loginFailed:(NSString *)errorMessage
{
    [self.activityView stopAnimating];
    [self showAlert:NSLocalizedString(@"ALERT",@"") message:errorMessage];
}
-(void)showAlert:(NSString *)title message:(NSString *)text
{
    UIAlertView *customAlert = [[UIAlertView alloc] initWithTitle:title message:text delegate:self cancelButtonTitle:NSLocalizedString(@"TRY_AGAIN_BUTTON", @"")  otherButtonTitles:nil];
    [customAlert show];
}
#pragma mark
#pragma Facebook Login Methods
- (IBAction)facebookButtonTouched:(id)sender{
    [self.activityView startAnimating];
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithReadPermissions:@[@"public_profile", @"email", @"user_friends"] handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if (error) {
            // Process error
        } else if (result.isCancelled) {
            // Handle cancellations
        } else {
            if ([result.grantedPermissions containsObject:@"email"]) {
                [self loginAllPass];
            }
        }
    }];
}

- (void)loginAllPass
{
    [self.activityView stopAnimating];
    __block NSString *email;
    if ([FBSDKAccessToken currentAccessToken]) {
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:nil]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
             if (!error) {
                 email = [result objectForKey:@"email"];
                 BTLoginDataSource *datasource = [[BTLoginDataSource alloc]init];
                 datasource.delegate = self;
                 [datasource login:[FBSDKProfile currentProfile].name
                                  :[FBSDKProfile currentProfile].lastName
                                  :email
                                  :[FBSDKProfile currentProfile].userID
                                  :[FBSDKAccessToken currentAccessToken].tokenString
                  ];
             }
         }];
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
}
-(BOOL)textFieldShouldReturn:(UITextField*)textField;
{
    NSInteger nextTag = textField.tag + 1;
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        [nextResponder becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }
    return NO;
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
@end
