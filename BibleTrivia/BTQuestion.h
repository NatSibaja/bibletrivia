//
//  BTQuestion.h
//  BibleTrivia
//
//  Created by Natalia Sibaja on 2/21/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BTQuestion : NSObject
@property (strong,nonatomic) NSNumber *questionId;
@property (strong,nonatomic) NSString *questionText;
@property (strong,nonatomic) NSString *choice1;
@property (strong,nonatomic) NSString *choice2;
@property (strong,nonatomic) NSString *choice3;
@property (strong,nonatomic) NSString *choice4;
@property (strong,nonatomic) NSNumber *answer;
@property (strong,nonatomic) NSString *answerText;
@property (strong,nonatomic) NSString *bibleReferenceText;
@property (strong,nonatomic) NSString *bibleReferenceUrl;
@property (strong,nonatomic) NSNumber *bookId;
@property (strong,nonatomic) NSNumber *categoryId;
@property (strong,nonatomic) NSNumber *chapter;
@property (strong,nonatomic) NSNumber *startVerse;
@property (strong,nonatomic) NSNumber *endVerse;


-(BOOL)isCorrect:(NSNumber *)choice;

@end
