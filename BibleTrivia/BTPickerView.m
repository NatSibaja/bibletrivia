//
//  BTPickerView.m
//  TriviaGame
//
//  Created by Natalia Sibaja on 5/16/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import "BTPickerView.h"
#import "BTBooksDataSource.h"
#import "BTBooksDelegate.h"

@interface BTPickerView()<UIPickerViewDelegate, UIPickerViewDataSource, BTBooksDelegate>
@property (strong, nonatomic) NSMutableArray *bookArray;
@property (strong, nonatomic) NSMutableArray *bookIdArray;
@property (strong, nonatomic) NSMutableArray *chapterArray;
@property (strong, nonatomic) NSMutableArray *chapterCount;
@property (strong, nonatomic) NSMutableArray *verseArray;
@property (strong, nonatomic) NSString *book, *chapter, *start, *end, *bibleReference;
@end

@implementation BTPickerView
@synthesize question;
@synthesize bookArray;
@synthesize bookIdArray;
@synthesize chapterArray;
@synthesize chapterCount;
@synthesize verseArray;

-(void)awakeFromNib
{
    [[NSBundle mainBundle] loadNibNamed:@"BTPicker" owner:self options:nil];
    [self setupBookPicker];
    self.contentView.frame = CGRectMake(0, 0, self.contentView.frame.size.width, self.contentView.frame.size.height);
    [self addSubview:self.contentView];
}
- (id)initWithFrame:(CGRect)rect{
    if(self = [super initWithFrame:rect]){
        [self awakeFromNib];
    }
    return self;
}
-(IBAction)cancelTouched:(id)sender
{
    [self.senderObject resignFirstResponder];
}
-(IBAction)doneTouched:(id)sender
{
    if (self.end.intValue == 1) {
            self.bibleReference = [NSString stringWithFormat:@"%@ %@:%@",self.book,self.chapter,self.start];
        }
        else{
            self.bibleReference = [NSString stringWithFormat:@"%@ %@:%@-%@",self.book,self.chapter,self.start,   self.end];
        }
    UITextField *reference = (UITextField *)self.senderObject;
    reference.text = self.bibleReference;
    self.question = [[BTQuestion alloc]init];
    self.question.bookId = [self.bookIdArray objectAtIndex:[self.bookArray indexOfObject:self.book]];
    self.question.chapter = [NSNumber numberWithInt:self.chapter.intValue];
    self.question.startVerse = [NSNumber numberWithInt:self.start.intValue];
    self.question.endVerse = [NSNumber numberWithInt:self.end.intValue];
    [self.senderObject resignFirstResponder];
}
-(void)setupBookPicker
{
    self.verseArray = [[NSMutableArray alloc]init];
    for (int i=1; i<=VERSES; i++) {
        [self.verseArray addObject:[NSNumber numberWithInt:i]];
    }
    self.pickerView.delegate = self;
    self.pickerView.dataSource = self;
    self.chapter= @"1";
    self.start= @"1";
    self.end = @"1";
    BTBooksDataSource *datasource = [[BTBooksDataSource alloc]init];
    datasource.delegate = self;
    [datasource requestBooks];
}
-(void)didReceiveBooks:(NSArray *)data
{
    self.bookArray  = [[NSMutableArray alloc]init];
    self.bookIdArray  = [[NSMutableArray alloc]init];
    self.chapterArray  = [[NSMutableArray alloc]init];
    self.chapterCount  = [[NSMutableArray alloc]init];
    
    for(int i=0;i<data.count;i++)
    {
        [self.bookArray addObject: [[data objectAtIndex:i] valueForKey:@"bookName"]];
        [self.bookIdArray addObject: [[data objectAtIndex:i] valueForKey:@"bookId"]];
        [self.chapterCount addObject: [[data objectAtIndex:i] valueForKey:@"chapterCount"]];
    }
    for (int i=0; i<self.chapterCount.count;i++) {
        NSMutableArray *chapterItems = [[NSMutableArray alloc]init];
        int count = [[self.chapterCount objectAtIndex:i]integerValue];
        for (int i=1; i<=count; i++) {
            [chapterItems addObject:[NSNumber numberWithInt:i]];
        }
        [self.chapterArray addObject:chapterItems];
    }
    self.book = [self.bookArray objectAtIndex:0];
}
-(void)didFailedReceivingBooks:(NSString *)errorMessage
{
    
}
#pragma mark
#pragma UIPickerView Methods

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 4;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    switch (component) {
        case 0:
            if (self.bookArray!=nil) {
                return [self.bookArray count];
            }
        case 1:
            if (self.book == nil)
            {
                NSString *string = [self.chapterCount objectAtIndex:0];
                return [string intValue]+1;
            }
            else
            {
                NSUInteger row = [self.bookArray indexOfObject:self.book];
                return [[self.chapterCount objectAtIndex:row] intValue];
            }
        case 2:
        case 3:
            return [self.verseArray count];
    }
    return 0;
}
-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    NSString *title = @"";
    if (self.bookArray != nil) {
        switch (component) {
            case 0:
                title = [self.bookArray objectAtIndex:row];
                break;
            case 1:
                if (self.book == nil) {
                    title = [NSString stringWithFormat:@"%@",[[self.chapterArray objectAtIndex:0]objectAtIndex:row]];
                }
                else{
                    title = [NSString stringWithFormat:@"%@",[[self.chapterArray objectAtIndex:[self.bookArray indexOfObject:self.book]]objectAtIndex:row]];
                }
                break;
            case 2:
            case 3:
                title = [NSString stringWithFormat:@"%@",[self.verseArray objectAtIndex:row]];
                break;
        }
    }
    NSDictionary *attributeDict = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:title attributes:attributeDict];
    
    UILabel *labelView = [[UILabel alloc] init];
    labelView.textAlignment = NSTextAlignmentCenter;
    labelView.attributedText = attributedString;
    
    return labelView;
}
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component{
    if (component == 0)
        return 120;
    return 60;
}
- (void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    switch (component) {
        case 0:
            self.book = [self.bookArray objectAtIndex:row];
            [self.pickerView reloadComponent:1];
            break;
        case 1:
            self.chapter = [[self.chapterArray objectAtIndex:[self.bookArray indexOfObject:self.book]]objectAtIndex:row];
            break;
        case 2:
            self.start = [self.verseArray objectAtIndex:row];
            break;
        case 3:
            self.end = [self.verseArray objectAtIndex:row];
            break;
    }
}
@end
