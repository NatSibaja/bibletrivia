//
//  Ad.m
//  BibleTrivia
//
//  Created by Leonardo Ortiz on 4/9/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import "Ad.h"
#import "Application.h"


@implementation Ad

@dynamic expiration;
@dynamic splashAd;
@dynamic bottomAd;
@dynamic application;

@end
