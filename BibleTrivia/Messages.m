//
//  Messages.m
//  TriviaGame
//
//  Created by Natalia Sibaja on 4/24/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import "Messages.h"


@implementation Messages

@dynamic message;
@dynamic timestamp;

@end
