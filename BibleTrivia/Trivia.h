//
//  Trivia.h
//  BibleTrivia
//
//  Created by Leonardo Ortiz on 4/9/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Trivia : NSManagedObject

@property (nonatomic, retain) NSNumber * bestRound;
@property (nonatomic, retain) NSNumber * score;
@property (nonatomic, retain) NSNumber * totalQuestions;

@end
