//
//  BTCustomActivityView.m
//  BibleTrivia
//
//  Created by Natalia Sibaja on 3/19/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import "BTCustomActivityView.h"
#import <QuartzCore/QuartzCore.h>
@implementation BTCustomActivityView

-(void)awakeFromNib
{
    [[NSBundle mainBundle] loadNibNamed:@"BTCustomActivityView" owner:self options:nil];
    UIImage *statusImage = [UIImage imageNamed:@"1.png"];
    self.activityImageView.image = statusImage;
    self.activityImageView.animationImages = [NSArray arrayWithObjects:
                                [UIImage imageNamed:@"1.png"],
                                [UIImage imageNamed:@"2.png"],
                                [UIImage imageNamed:@"3.png"],
                                [UIImage imageNamed:@"4.png"],
                                [UIImage imageNamed:@"5.png"],
                                [UIImage imageNamed:@"6.png"],
                                [UIImage imageNamed:@"7.png"],
                                nil];
    self.activityImageView.animationDuration = 0.7;
    //Start the animation
    self.content.layer.cornerRadius = 10;
    self.content.layer.borderColor = [UIColor whiteColor].CGColor;
    self.content.layer.borderWidth = 1;
    self.content.hidden = YES;
    [self addSubview:self.content];
}
-(void)startAnimating
{
    self.content.hidden = NO;
    [self.activityImageView startAnimating];
}
-(void)stopAnimating
{
    [self.activityImageView stopAnimating];
    self.content.hidden = YES;
}
@end
