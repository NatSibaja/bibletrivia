//
//  NSDate+BTDate.m
//  BibleTrivia
//
//  Created by Natalia Sibaja on 4/7/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import "NSDate+BTDate.h"

@implementation NSDate (BTDate)

+(NSDate *)dateByAddingHours:(NSInteger) hours toDate:(NSDate *)date{
    NSDateComponents *dayComponent = [[NSDateComponents alloc]init];
    dayComponent.hour = hours;
    NSCalendar *currentCalendar = [NSCalendar currentCalendar];
    date = [currentCalendar dateByAddingComponents:dayComponent toDate:date options:0];
    return date;
}
+ (int)daysFromDate:(NSDate *)pDate {
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSInteger startDay=[calendar ordinalityOfUnit:NSCalendarUnitDay
                                           inUnit:NSCalendarUnitEra
                                          forDate:[NSDate date]];
    NSInteger endDay=[calendar ordinalityOfUnit:NSCalendarUnitDay
                                         inUnit:NSCalendarUnitEra
                                        forDate:pDate];
    return abs(endDay-startDay);
}
@end
