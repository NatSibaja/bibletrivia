//
//  BTMessageCenterViewController.m
//  TriviaGame
//
//  Created by Natalia Sibaja on 5/20/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import "BTMessageCenterViewController.h"
#import "BTMainViewController.h"

@interface BTMessageCenterViewController ()

@end

@implementation BTMessageCenterViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
   
   
}
-(void)viewWillAppear:(BOOL)animated
{
    UAInboxMessageListController *mlc = [self buildMessageListController];
    [self.navigationController pushViewController:mlc animated:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (UAInboxMessageListController *)buildMessageListController {
    UAInboxMessageListController *mlc = [[UAInboxMessageListController alloc] initWithNibName:@"UAInboxMessageListController" bundle:nil];
    mlc.title = @"Inbox";
    
    mlc.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                                                         target:self
                                                                                         action:@selector(inboxDone:)];
    
    //the closeBLock allows for rich push messages to close the inbox after running actions
    mlc.closeBlock = ^(BOOL animated) {
        [self hideInbox];
    };
    
    return mlc;
}
- (void)inboxDone:(id)sender {
    [self hideInbox];
}
- (void)hideInbox {
    [self performSegueWithIdentifier:@"mainSegue" sender:self];
}
@end
