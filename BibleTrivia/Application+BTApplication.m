//
//  Application+BTApplication.m
//  BibleTrivia
//
//  Created by Leonardo Ortiz on 3/24/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import "Application+BTApplication.h"
#import "BTManagedObjectContext.h"

@implementation Application (BTApplication)
-(void)save{
    NSError *error = nil;
    if (![[BTManagedObjectContext sharedInstance].managedObjectContext  save:&error]) {
        if (DEBUG_COREDATA) {
            NSLog(@"CoreData:Error saving the Application.%@",error);
        }
    }
}

-(void)erase{
    NSError *error = nil;
    [[BTManagedObjectContext sharedInstance].managedObjectContext deleteObject:self];
    if (![[BTManagedObjectContext sharedInstance].managedObjectContext  save:&error]) {
        if (DEBUG_COREDATA) {
            NSLog(@"CoreData:Failed deleting the Application.%@",error);
        }
    }
}

@end
