//
//  BTSidebarViewController.m
//  BibleTrivia
//
//  Created by Natalia Sibaja on 3/17/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//
typedef NS_ENUM(NSInteger, InboxStyle) {
    InboxStyleModal,
    InboxStyleNavigation
};


#import "BTSidebarViewController.h"
#import "SWRevealViewController.h"
#import "BTUserDataSource.h"
#import "User+BTUser.h"
#import <UIKit/UIKit.h>
#import "UAirship.h"
#import "UAInbox.h"
#import "InboxSampleNavigationUserInterface.h"
#import "UAUtils.h"
#import "BTMainViewController.h"


@interface BTSidebarViewController ()
@property (nonatomic, strong) NSArray *menuItems;
@property(nonatomic, assign) InboxStyle style;
@property(nonatomic, strong) InboxSampleNavigationUserInterface *userInterface;
@property(nonatomic, strong) UAInboxAlertHandler *alertHandler;

-(BOOL)userExists;
@end

@implementation BTSidebarViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
  //  IBUserDataSource *datasource = [[IBUserDataSource alloc]init];
    //self.user = [datasource getUserFromContext];
    
    self.view.backgroundColor = SIDE_GRAY;
    [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    self.tableView.separatorColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"cell-divider"]];
    [self.tableView setTableFooterView:[UIView new]];
    _menuItems = @[@"play",@"question",@"scores",@"message",@"rate",@"account"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void) prepareForSegue: (UIStoryboardSegue *) segue sender: (id) sender
{
    // configure the segue.
    if ( [segue isKindOfClass: [SWRevealViewControllerSegue class]] )
    {
        SWRevealViewControllerSegue* rvcs = (SWRevealViewControllerSegue*) segue;
        
        SWRevealViewController* rvc = self.revealViewController;
        NSAssert( rvc != nil, @"oops! must have a revealViewController" );
        
        NSAssert( [rvc.frontViewController isKindOfClass: [UINavigationController class]], @"oops!  for this segue we want a permanent navigation controller in the front!" );
        
        rvcs.performBlock = ^(SWRevealViewControllerSegue* rvc_segue, UIViewController* svc, UIViewController* dvc)
        {
            UINavigationController* nc = [[UINavigationController alloc] initWithRootViewController:dvc];
            [rvc pushFrontViewController:nc animated:YES];
        };
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.menuItems count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = [self.menuItems objectAtIndex:indexPath.row];
 
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    if (![self userExists]) {
        if(indexPath.row !=5){
        cell.userInteractionEnabled = NO;
        UILabel *titleLabel = (UILabel *)[cell viewWithTag:1];
        titleLabel.textColor = [UIColor darkGrayColor];
        }
    }
    
    cell.backgroundColor = CELL_GRAY;
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 4) {
        [self rateApp];
    }
    if (indexPath.row == 3) {
        self.userInterface.revealController = self.revealViewController;
        [self.userInterface showInbox];
    }
}

- (void)rateApp {
    BOOL neverRate = [[NSUserDefaults standardUserDefaults] boolForKey:@"neverRate"];
    
    if (neverRate != YES) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Rate Our App"
                                                        message:@"Enjoying Bible Trivia?"
                                                       delegate:self
                                              cancelButtonTitle:@"Maybe Later"
                                              otherButtonTitles:@"Yes, Rate in iTunes", @"No, Leave feedback", nil];
        alert.delegate = self;
        [alert show];
    }
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"neverRate"];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:ITUNES_URL]]];
        
    }
    else if (buttonIndex == 2) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"neverRate"];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:FEEDBACK_URL]]];
    }
}
-(BOOL)userExists
{
    BTUserDataSource *datasource = [[BTUserDataSource alloc]init];
    datasource.delegate = self;
    User *user = [datasource getUserFromContext];
    if (user != nil)
        return YES;
    else
        return NO;
}

#pragma mark
#pragma URBAN AIRSHIP

- (void)awakeFromNib {
    self.alertHandler = [[UAInboxAlertHandler alloc] init];
}

/*
 Builds a new instance of the message list controller, configuring buttons and closeBlock implemenations.
 */
- (UAInboxMessageListController *)buildMessageListController {
    UAInboxMessageListController *mlc = [[UAInboxMessageListController alloc] initWithNibName:@"UAInboxMessageListController" bundle:nil];
    mlc.title = @"Message Center";
    
    mlc.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                                                         target:self
                                                                                         action:@selector(inboxDone:)];
    
    //the closeBLock allows for rich push messages to close the inbox after running actions
    mlc.closeBlock = ^(BOOL animated) {
        [self.userInterface hideInbox];
    };
    
    return mlc;
}

- (void)inboxDone:(id)sender {
    [self.userInterface hideInbox];
}

/*
 Displays an incoming message, either by showing it in an overlay,
 or loading it in an already visible inbox interface.
 */
- (void)displayMessage:(UAInboxMessage *)message {
    if (![self.userInterface isVisible]) {
        if (self.useOverlay) {
            [UALandingPageOverlayController showMessage:message];
            return;
        } else {
            [self.userInterface showInbox];
        }
    }
    
    [self.userInterface.messageListController displayMessage:message];
}

- (void)setStyle:(enum InboxStyle)style {
    UAInboxMessageListController *mlc = [self buildMessageListController];
    switch (style) {
        case InboxStyleNavigation:
            self.userInterface = [[InboxSampleNavigationUserInterface alloc] initWithMessageListController:mlc];
            break;
        default:
            break;
    }
    
    self.userInterface.parentController = self;
    _style = style;
}

#pragma mark UAInboxPushHandlerDelegate methods

/*
 Called when a new rich push message is available for viewing.
 */
- (void)richPushMessageAvailable:(UAInboxMessage *)message {
    
    // Display an alert, and if the user taps "View", display the message
    NSString *alertText = message.title;
    [self.alertHandler showNewMessageAlert:alertText withViewBlock:^{
        [self displayMessage:message];
    }];
}
/*
 Called when a new rich push message is available after launching from a
 push notification.
 */
- (void)launchRichPushMessageAvailable:(UAInboxMessage *)message {
    [self displayMessage:message];
}

- (void)richPushNotificationArrived:(NSDictionary *)notification {
    // Add custom notification handling here
}

- (void)applicationLaunchedWithRichPushNotification:(NSDictionary *)notification {
    // Add custom launch notification handling here
}
-(void)showInbox
{}
-(void)showInboxMessage:(UAInboxMessage *)inboxMessage
{}

@end
