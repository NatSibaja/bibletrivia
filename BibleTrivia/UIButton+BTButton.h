//
//  UIButton+BTButton.h
//  BibleTrivia
//
//  Created by Natalia Sibaja on 3/14/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (BTButton)
-(void)fillGradientBackground:(UIColor* )startColor :(UIColor *)endColor;
@end
