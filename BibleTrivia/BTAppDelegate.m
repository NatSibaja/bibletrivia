//
//  BTAppDelegate.m
//  BibleTrivia
//
//  Created by Natalia Sibaja on 2/19/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import "BTAppDelegate.h"
#import "BTMainViewController.h"
#import "BTManagedObjectContext.h"
#import "BTLaunchViewController.h"
#import "BTBannerView.h"
#import "Messages.h"
#import "BTMessagesDataSource.h"
#import "NSDate+BTDate.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <AirshipKit/AirshipKit.h>

@interface BTAppDelegate()<UAPushNotificationDelegate>
@property (nonatomic, weak) BTMainViewController *viewController;

-(void)initializeFlurry;
-(void)initializeGoogleAnalytics;

@end
@implementation BTAppDelegate

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    NSManagedObjectContext *context = [self managedObjectContext];
    BTManagedObjectContext *btManagedObjectContext = [BTManagedObjectContext sharedInstance];
    btManagedObjectContext.managedObjectContext = context;
    BTLaunchViewController *vc = [[BTLaunchViewController alloc]init];
    [[launchOptions valueForKey:UIApplicationLaunchOptionsRemoteNotificationKey] description];
    self.customLoginViewController = vc;
    [self initializeFlurry];
    [Flurry logEvent:@"Bible_Trivia_Launched"];
    [self initializeUrbanAirship:launchOptions];
    [self initializeAdvertising];
    [self initializeGoogleAnalytics];
    [self initializeRating];
    [FBSDKProfile enableUpdatesOnAccessTokenChange:YES];
    return [[FBSDKApplicationDelegate sharedInstance] application:application didFinishLaunchingWithOptions:launchOptions];
    return YES;
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    [FBSDKAppEvents activateApp];
    // Do the following if you use Mobile App Engagement Ads to get the deferred
    // app link after your app is installed.
    [FBSDKAppLinkUtility fetchDeferredAppLink:^(NSURL *url, NSError *error) {
        if (error) {
            NSLog(@"Received error while fetching deferred app link %@", error);
        }
        if (url) {
            [[UIApplication sharedApplication] openURL:url];
        }
    }];
}
-(void)initializeUrbanAirship:(NSDictionary *)launchOptions
{
    UAConfig *config = [UAConfig defaultConfig];
    [UAirship takeOff:config];
    [UAirship push].userPushNotificationsEnabled = YES;
}

#pragma mark
#pragma Analytics Initialization 
-(void)initializeFlurry{
    if (FLURRY_ENABLED) {
        [Flurry startSession:FLURRY_KEY];
    }
}

-(void)initializeGoogleAnalytics{
    if (GOOGLE_ANALYTICS_ENABLED) {
        [GAI sharedInstance].trackUncaughtExceptions = YES;
        [GAI sharedInstance].dispatchInterval = GOOGLE_ANALYTICS_DISPATH_INTERVAL;
        [[GAI sharedInstance] trackerWithTrackingId:GOOGLE_ANALYTICS_KEY];
    }
    
}
- (void) initializeAdvertising
{
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"firstRun"] == nil) {
        [[NSUserDefaults standardUserDefaults] setBool:TRUE forKey:@"firstRun"];
    }
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"firstRun"])
    {
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"adTimeout"] == nil)
        {
            NSDate *yesterday = [[NSDate date] dateByAddingTimeInterval:60*60*24*-1];
            [[NSUserDefaults standardUserDefaults] setObject:yesterday forKey:@"adTimeout"];
        }
    }
}
-(void)initializeRating
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    //Ask for Rating
    BOOL neverRate = [prefs boolForKey:@"neverRate"];

    int launchCount = 0;
    //Get the number of launches
    launchCount = [prefs integerForKey:@"launchCount"];
    launchCount++;
    [[NSUserDefaults standardUserDefaults] setInteger:launchCount forKey:@"launchCount"];
    
    if (!neverRate)
    {
        if ( (launchCount == 3) || (launchCount == 9) || (launchCount == 15) || (launchCount == 21) )
        {
            [self rateApp];
        }
    }
    [prefs synchronize];
}
- (void)rateApp {
    BOOL neverRate = [[NSUserDefaults standardUserDefaults] boolForKey:@"neverRate"];
    
    if (neverRate != YES) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Rate Our App"
                                                        message:@"Enjoying Bible Trivia?"
                                                       delegate:self
                                              cancelButtonTitle:@"Maybe Later"
                                              otherButtonTitles:@"Yes, Rate in iTunes", @"No, Leave feedback", nil];
        alert.delegate = self;
        [alert show];
    }
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"neverRate"];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:ITUNES_URL]]];

        }
    else if (buttonIndex == 2) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"neverRate"];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:FEEDBACK_URL]]];
    }
}
- (void)showPopUp
{
        NSDate *today = [[NSDate alloc]init];
        [[NSUserDefaults standardUserDefaults] setObject:today forKey:@"adTimeout"];
        [[NSUserDefaults standardUserDefaults] setBool:FALSE forKey:@"firstRun"];
        BTBannerView *splash = [[BTBannerView alloc]init];
        splash.bannerType = 1;
        splash.viewController = self.window.rootViewController;
        [self.window.rootViewController.view addSubview:splash];
        [splash showAd];
  
}
-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Set the application's badge to the number of unread messages
    if ([UAirship inbox].messageList.unreadCount >= 0) {
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:[UAirship inbox].messageList.unreadCount];
    }
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"firstRun"] ) {
    NSDate *savedDate = [[NSUserDefaults standardUserDefaults] objectForKey:@"adTimeout"];
    NSDateComponents *dateComponents = [[NSCalendar currentCalendar] components:NSCalendarUnitDay fromDate:savedDate toDate:[NSDate date] options:0];
    if ([dateComponents day] >= 1)
    {
        [self showPopUp];
    }
    }
    else
    {
        [[NSUserDefaults standardUserDefaults] setBool:FALSE forKey:@"firstRun"];
    }
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}
- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
             // Replace this implementation with code to handle the error appropriately.
             // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        } 
    }
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"BibleTrivia" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"BibleTrivia.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
       
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }    
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}
@end
