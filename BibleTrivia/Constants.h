//
//  Constants.h
//  BibleTrivia
//
//  Created by Natalia Sibaja on 2/19/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#ifndef BibleTrivia_Constants_h
#define BibleTrivia_Constants_h

//Debug values
#define DEBUG_ENABLED                       YES
#define DEBUG_URLRESQUESTS                  YES
#define DEBUG_QUESTIONS                     YES
#define DEBUG_COREDATA                      YES

//Analytics
#define FLURRY_ENABLED                      YES
#define FLURRY_KEY                          @"7XNZK9Z5HQKWG6T3RGYM"
#define GOOGLE_ANALYTICS_ENABLED            YES
#define GOOGLE_ANALYTICS_KEY                @"UA-11861759-22"
#define GOOGLE_ANALYTICS_DISPATH_INTERVAL   20
#endif

//colors
#define BLUE               [UIColor colorWithRed:51/255.0f green:182/255.0f blue:255/255.0f alpha:1.0f];
#define BLUE_BUTTON        [UIColor colorWithRed:0 green:0.541 blue:0.922 alpha:1.0]
#define DARK_GREEN         [UIColor colorWithRed:0.443 green:0.925 blue:0.506 alpha:1.0]
#define LIGHT_GREEN        [UIColor colorWithRed:0.259 green:0.745 blue:0.69 alpha:1.0]
#define SIDE_GRAY          [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0]
#define CELL_GRAY          [UIColor colorWithRed:0.251 green:0.251 blue:0.251 alpha:1.0]
#define BLUE_FADE          [UIColor colorWithRed:0 green:0.4 blue:0.8 alpha:0.1]

#define isiPhone5  ([[UIScreen mainScreen] bounds].size.height == 568)?TRUE:FALSE

//Ads
#define MY_AD_UNIT_ID                       @"/6177/app.bibletrivia"
#define MY_INTERSTITIAL_UNIT_ID             @"/6177/app.bibletrivia/interstitial"
#define SPLASH_TIMEOUT                      24
#define ADDELAY                             8 //seconds ad will run
//API

//DEV API URL
#define API_URL                             @"https://bibletrivia-api.salemwebnetwork.com/"
//#define API_KEY                             @"90F1B9F1-00AD-4E1B-8C18-B2062467693F"
//PRODUCTION KEY
#define API_KEY                             @"7D803062-E676-4639-877E-301CE8A95F89"

#define LOGIN_URL                           @"login"
#define USER_URL                            @"users"
#define USER_STATS_URL                      @"userstats"
#define QUESTION_URL                        @"questions"
#define BOOKS_URL                           @"books?page=1&pagesize=100"
#define ANSWERS_URL                         @"answers"
#define NEWSLETTER_URL                      @"newsletters"
#define FORGOT_URL                          @"https://www.salemallpass.com/forgot-password"
#define TERMS_URL                           @"http://www.salemwebnetwork.com/our-brands/terms-of-use/"
#define ITUNES_URL                          @"itms-apps://itunes.apple.com/app/id563525027"
#define FEEDBACK_URL                        @"http://appfeedback.salemwebnetwork.com/Bible-Trivia/iOS"
#define NO_QUESTIONS                        20
#define VERSES                              176

//Social
#define FB_SHARE_URL                        @"http://bit.ly/1lAtQro"
#define TWITTER_SHARE_URL                   @"http://bit.ly/1lAtQro"
#define SHARE_IMAGE                         @"iTunesArtwork"
#define FB_SHARE_IMAGE                      @"http://i58.tinypic.com/2mn3ghd.png"






