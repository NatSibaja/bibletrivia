//
//  BTSocial.m
//  BibleTrivia
//
//  Created by Leonardo Ortiz on 3/28/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import "BTSocial.h"
#import <Social/Social.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>

#define FB_ERROR_ALERT      0
#define TWITTER_ERROR_ALERT 1

@interface BTSocial()
-(void)showErrorMessage:(int)tag;
@end
@implementation BTSocial


-(void)shareToFacebook:(NSString *)initialText andImage:(UIImage *)image andUrl:(NSURL *)url{
        SLComposeViewController *fbComposerView  = [[SLComposeViewController alloc] init];
        fbComposerView = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        [fbComposerView setInitialText:initialText];
        [fbComposerView addImage:image];
        [fbComposerView addURL:url];
        [self.rootView presentViewController:fbComposerView animated:YES completion:nil];
}

// A function for parsing URL parameters returned by the Feed Dialog.
- (NSDictionary*)parseURLParams:(NSString *)query {
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    for (NSString *pair in pairs) {
        NSArray *kv = [pair componentsSeparatedByString:@"="];
        NSString *val =
        [kv[1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        params[kv[0]] = val;
    }
    return params;
}
-(void)shareFacebook{
    [self shareToFacebook:NSLocalizedString(@"SHARE_FB_MESSAGE", @"")
                 andImage:[UIImage imageNamed:SHARE_IMAGE]
                   andUrl:[NSURL URLWithString:FB_SHARE_URL]];
}

-(void)shareToTwitter:(NSString *)initialText andImage:(UIImage *)image andUrl:(NSURL *)url{
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        SLComposeViewController *twitterComposerView = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        [twitterComposerView setInitialText:initialText];
        [twitterComposerView addImage:image];
        [twitterComposerView addURL:url];
        [self.rootView presentViewController:twitterComposerView animated:YES completion:nil];
        
    }
    else{
        [self showErrorMessage:TWITTER_ERROR_ALERT];
    }
}

-(void)shareToTwitter{
    [self shareToTwitter:NSLocalizedString(@"SHARE_TWITTER_MESSAGE", @"")
                andImage:[UIImage imageNamed:SHARE_IMAGE]
                  andUrl:[NSURL URLWithString:TWITTER_SHARE_URL]];
}

-(void)showErrorMessage:(int)tag{
    NSString *message = nil;
    switch (tag) {
        case FB_ERROR_ALERT:
            message = NSLocalizedString(@"SHARE_FB_ERROR", @"");
            break;
        case TWITTER_ERROR_ALERT:
            message = NSLocalizedString(@"SHARE_TWITTER_ERROR", @"");
            break;
        default:
            break;
    }
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error"
                                                   message:message
                                                  delegate:nil
                                         cancelButtonTitle:NSLocalizedString(@"ACCEPT_BUTTON", @"")
                                         otherButtonTitles:nil];
    alert.tag = tag;
    [alert show];
}
@end
