//
//  BTSettings.h
//  TriviaGame
//
//  Created by Natalia Sibaja on 4/7/15.
//  Copyright (c) 2015 Salem Web Network. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BTSettings : NSObject

+ (instancetype)defaultSettings;

@property (nonatomic, assign) BOOL shouldSkipLogin;

@end