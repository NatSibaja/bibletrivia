//
//  Messages+BTMessages.h
//  TriviaGame
//
//  Created by Natalia Sibaja on 4/25/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import "Messages.h"

@interface Messages (BTMessages)
-(void)save;
-(void)erase;
@end
