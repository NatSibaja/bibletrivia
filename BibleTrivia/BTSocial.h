//
//  BTSocial.h
//  BibleTrivia
//
//  Created by Leonardo Ortiz on 3/28/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BTSocial : NSObject
//Reference to the view who calls it
@property (strong, nonatomic) UIViewController *rootView;
-(void)shareToFacebook:(NSString *)initialText andImage:(UIImage *)image andUrl:(NSURL *)url;
-(void)shareFacebook;
-(void)shareToTwitter:(NSString *)initialText andImage:(UIImage *)image andUrl:(NSURL *)url;
-(void)shareToTwitter;
@end
