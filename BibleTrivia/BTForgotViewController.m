//
//  BTForgotViewController.m
//  TriviaGame
//
//  Created by Natalia Sibaja on 4/30/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import "BTForgotViewController.h"
#import "BTCustomActivityView.h"

@interface BTForgotViewController ()

<UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet BTCustomActivityView *activityView;
@end

@implementation BTForgotViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.activityView startAnimating];
    self.webView.delegate = self;
    NSURL *websiteUrl = [NSURL URLWithString: FORGOT_URL];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:websiteUrl];
    [self.webView loadRequest:urlRequest];
}
-(void)viewWillAppear:(BOOL)animated
{
    self.screenName = @"Forgot Password";
    [Flurry logEvent:@"Forgot Password"];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma WebView Delegate
-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self.activityView stopAnimating];
}
@end
