//
//  BTSplashAdViewController.m
//  BibleTrivia
//
//  Created by Natalia Sibaja on 4/9/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import "BTSplashAdViewController.h"
#import "BTAdvertisingDataSource.h"
#import "Ad+BTAd.h"

//Defines how many seconds the splash screen will be shown

@interface BTSplashAdViewController ()
@property (weak, nonatomic) IBOutlet UIButton *closeButton;
@property (nonatomic,assign) BOOL cancelTransition;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightConstraint;
@property (nonatomic, weak) NSString *segue;
@end

@implementation BTSplashAdViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setNavigationBar];
    [self setAdSize];
    if ([self showAd]) {
        self.cancelTransition = NO;
        [self performSelector:@selector(doTransition:) withObject:self.segue afterDelay:ADDELAY];
    }
}
-(void)viewWillAppear:(BOOL)animated
{
    self.screenName = @"Pop Up Ad";
    [Flurry logEvent:@"Pop Up Ad"];
}
-(BOOL)prefersStatusBarHidden { return YES; }
-(void)viewWillDisappear:(BOOL)animated
{
    [[UIApplication sharedApplication]setStatusBarHidden:NO withAnimation:NO];
    self.navigationController.navigationBar.hidden = NO;

}
-(BOOL)showAd
{
    return YES;
}
-(void)setNavigationBar
{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationController.navigationBar.hidden = YES;
    [[UIApplication sharedApplication]setStatusBarHidden:YES withAnimation:NO];
}
-(void)setAdSize
{
    CGRect frame = self.view.frame;
    self.heightConstraint.constant = frame.size.height;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)doTransition:(NSString *) segue
{
    if (!self.cancelTransition) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}
-(IBAction)closeAd:(id)sender
{
    self.cancelTransition = YES;
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
