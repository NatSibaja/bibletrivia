//
//  BTBannerView.m
//  BibleTrivia
//
//  Created by Natalia Sibaja on 4/7/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import "BTBannerView.h"
#import "BTAdvertisingDataSource.h"
#import "Ad.h"
#import "Application.h"
#import "BTApplicationDataSource.h"

@interface BTBannerView()<UIWebViewDelegate, GADInterstitialDelegate>
-(void)showAd;
@end
@implementation BTBannerView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [super awakeFromNib];
    }
    return self;
}
-(void)awakeFromNib
{
    switch (self.bannerType) {
        case 0:
            [[NSBundle mainBundle] loadNibNamed:@"BTBannerView" owner:self options:nil];
            break;
        case 1:
            [[NSBundle mainBundle] loadNibNamed:@"BTSplashBannerView" owner:self options:nil];
            break;
        default:
            break;
    }
    [self addSubview:self.contentView];
    self.contentView.backgroundColor = BLUE;
    [self showAd];
}

#pragma mark - Receive Ad Data
-(void)changeAd{
    [self showAd];
}
-(void)showAd
{
    if (self.bannerType == 0) {
        self.bannerView_ = [[DFPBannerView alloc] initWithAdSize:kGADAdSizeBanner];
        self.bannerView_.adUnitID = MY_AD_UNIT_ID;
        [self addSubview:self.bannerView_];
        self.bannerView_.rootViewController = self.viewController;
        [self.bannerView_ loadRequest:[GADRequest request]];
    }
    else if(self.bannerType == 1)
    {
        self.interstitial_ = [[GADInterstitial alloc] init];
        self.interstitial_.adUnitID = MY_INTERSTITIAL_UNIT_ID;
        self.interstitial_.delegate = self;
        
        GADRequest *request = [GADRequest request];
        [self.interstitial_ loadRequest:request];
        UIImageView * imageView_ = [[UIImageView alloc] initWithImage:[UIImage  imageNamed:@"InitialImage"]];
        [self addSubview:imageView_];
    }
}

// Initialize your splash image and add it to the view.
- (void)interstitialDidReceiveAd:(GADInterstitial *)ad {
    NSLog(@"Received ad successfully");
    [self.interstitial_ presentFromRootViewController:self.viewController];
}
- (void)interstitial:(GADInterstitial *)ad
didFailToReceiveAdWithError:(GADRequestError *)error {
    NSLog(@"Failed to receive ad with error: %@", [error localizedFailureReason]);
    //[self restoreController];
}

- (void)interstitialWillDismissScreen:(GADInterstitial *)ad {
    // Remove the imageView_ once the interstitial is dismissed.
    //[self restoreController];
}


@end
