//
//  Trivia+BTTrivia.h
//  BibleTrivia
//
//  Created by Natalia Sibaja on 3/31/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import "Trivia.h"

@interface Trivia (BTTrivia)
-(void)save;
-(void)erase;
-(void)saveBestRound:(NSNumber *)newScore;
@end
