//
//  BTAdvertisingDataSource.m
//  BibleTrivia
//
//  Created by Natalia Sibaja on 4/7/14.
//  Copyright (c) 2014 Salem Web Network. All rights reserved.
//

#import "BTAdvertisingDataSource.h"
#import "BTManagedObjectContext.h"
#import "BTApplicationDataSource.h"
#import "Ad.h"
#import "Ad+BTAd.h"
#import "NSDate+BTDate.h"
#import "BTUrlSession.h"
#import "Application.h"
#import "Application+BTApplication.h"
#pragma GCC diagnostic ignored "-Wundeclared-selector"

@interface BTAdvertisingDataSource()
@property (nonatomic,strong) Ad *splashAd;
-(BOOL)areAdsValid;
-(Ad *)insertAdInContext;
@end

@implementation BTAdvertisingDataSource


-(void)requestAds{
    /*if (![self areAdsValid]) {
        BTUrlSession *btUrlSession = [[BTUrlSession alloc]init];
        btUrlSession.delegate=self;
        btUrlSession.didReceiveData = NSSelectorFromString(@"didReceiveAdsinfo:");
        btUrlSession.didReceiveError = NSSelectorFromString(@"didReceiveAdsinfoError:");
//NSString *stringUrl = [NSString stringWithFormat:@"%@",ADS_URL];
        [btUrlSession doGetRequest:stringUrl withParams:nil andHeaders:@{@"Content-Type":@"application/text"}];
    }
    else{
        Application *app = [[[BTApplicationDataSource alloc]init]getApplication];
        if ([self.delegate respondsToSelector:@selector(didReceiveAds:)]) {
            [self.delegate performSelector:@selector(didReceiveAds:) withObject:app.ads];
        }
    }
     */
}


-(void)didReceiveAdsinfo:(NSData *)data{
    Application *app = [[[BTApplicationDataSource alloc]init]getApplication];
    Ad *ad = [self insertAdInContext];
    NSError *error;
    NSDictionary *dataDicc = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    if (error==nil) {
        dataDicc = [dataDicc objectForKey:@"data"];
        NSArray  *adsArray = [dataDicc objectForKey:@"ads"];
        for (NSDictionary *adDicc in adsArray) {
            if ([[adDicc objectForKey:@"position"]isEqualToString:@"bottom"]) {
                ad.bottomAd = [adDicc objectForKey:@"urlOrSnippet"];
            }
            else{
                ad.splashAd = [adDicc objectForKey:@"urlOrSnippet"];
                NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                //[prefs setObject:[NSDate dateByAddingHours:SPLASH_TIMEOUT toDate:[NSDate date]] forKey:@"adTimeout"];
                [prefs synchronize];
            }
        }
        ad.expiration = [NSDate dateByAddingHours:SPLASH_TIMEOUT toDate:[NSDate date]];
      
        app.ads = ad;
        [app save];
        if ([self.delegate respondsToSelector:@selector(didReceiveAds:)]) {
            [self.delegate performSelector:@selector(didReceiveAds:) withObject:app.ads];
        }
        
    }
    else{
        if ([self.delegate respondsToSelector:@selector(didReceiveAdsError:)]) {
            [self.delegate performSelector:@selector(didReceiveAdsError:) withObject:@"Error getting the parsing the ads info"];
        }
    }
    
    
    
}



-(void)didReceiveAdsinfoError:(NSUrlErrorType *)error{
    if ([self.delegate respondsToSelector:@selector(didReceiveAdsError:)]) {
        [self.delegate performSelector:@selector(didReceiveAdsError:) withObject:@"Error getting the ads info"];
    }
}




-(BOOL)areAdsValid{
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"Ad" inManagedObjectContext:[BTManagedObjectContext sharedInstance].managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    [request setEntity:entityDescription];
    NSError *error;
    NSArray *mutableFetchResults = [[BTManagedObjectContext sharedInstance].managedObjectContext executeFetchRequest:request error:&error];
    if ([mutableFetchResults count]>0) {
        Ad * ad = (Ad *) [mutableFetchResults objectAtIndex:0];
        if (ad.isValid == YES) {
            return YES;
        }
        else{
            //The ad is no longer useful. Erase it
            [ad erase];
            return NO;
        }
    }
    else{
        return NO;
    }
}

-(Ad *)insertAdInContext{
    Application *application = [[[BTApplicationDataSource alloc]init]getApplication];
    Ad *ad = [NSEntityDescription insertNewObjectForEntityForName:@"Ad"
                                            inManagedObjectContext:[BTManagedObjectContext sharedInstance].managedObjectContext];
    ad.application = application;
    return ad;
}

@end